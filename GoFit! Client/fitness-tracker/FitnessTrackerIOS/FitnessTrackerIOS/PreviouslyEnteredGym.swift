//
//  PreviouslyEnteredGym.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/30/15.
//  Copyright © 2015 edu. All rights reserved.
//

import Foundation
import CoreData

// This class represents the gym selected by the user(after entering zipcode)
class PreviouslyEnteredGym: NSManagedObject {
    
    @NSManaged var gymAddress: String?
    @NSManaged var gymCity: String?
    @NSManaged var gymID: String?
    @NSManaged var gymName: String?
    @NSManaged var gymOwner: String?
    @NSManaged var gymState: String?
    @NSManaged var gymZipcode: String?
}
