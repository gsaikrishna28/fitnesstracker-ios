//
//  MachineDetailsAddedFromParse.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/24/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import Foundation
import CoreData

class MachineDetailsAddedFromParse: NSManagedObject {

    @NSManaged var gymName: String
    @NSManaged var machinesAdded: NSNumber

}
