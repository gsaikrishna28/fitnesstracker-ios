//
//  Exercise.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/23/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import Foundation
import CoreData

// This class represents the information that is stored by each exercise
class Exercise: NSManagedObject {

    @NSManaged var exerciseID: NSNumber
    @NSManaged var exerciseName: String
    @NSManaged var exerciseSelected: NSNumber
    @NSManaged var numOfReps: NSNumber
    @NSManaged var numOfSets: NSNumber
    @NSManaged var restTime: NSDecimalNumber
    @NSManaged var seatingPosition: NSNumber
    @NSManaged var weight: NSDecimalNumber
    @NSManaged var belongsToMachine: Machine

}
