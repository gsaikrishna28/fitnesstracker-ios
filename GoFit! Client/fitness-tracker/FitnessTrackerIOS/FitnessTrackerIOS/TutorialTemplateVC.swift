//
//  ContentViewController.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 7/5/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData

// This class represents the template for the TutorialVC
class TutorialTemplateVC: UIViewController {

    // Each page in the tutorial contains: title, image, button(to skip the tutorial)
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var skipBTN: UIButton!
    
    var pageIndex : Int!
    var titleText : String!
    var imageFile : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.image = UIImage(named: self.imageFile)
        
    }
    
    // Called when the user clicks on "Skip Tutorial" button
    @IBAction func skipTutorial(sender: AnyObject) {
        
        let fetch = NSFetchRequest(entityName: "PreviouslyEnteredGym")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gyms = ((try! (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext!.executeFetchRequest(fetch)) as! [PreviouslyEnteredGym])
        if gyms.count > 0 {
            self.presentViewController(storyboard.instantiateViewControllerWithIdentifier("tabBarController") as! UITabBarController , animated: true, completion: nil)
        }
        else {
        
            let searchingGymsNVC = self.storyboard?.instantiateViewControllerWithIdentifier("searchingGymsNVC") as! UINavigationController
            self.presentViewController(searchingGymsNVC, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if pageIndex == 8 {
            skipBTN.setTitle("   Got It   ", forState: .Normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
