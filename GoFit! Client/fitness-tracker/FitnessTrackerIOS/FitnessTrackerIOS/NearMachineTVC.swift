//
//  NearMachineTVC.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 7/22/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData
import Parse
import Bolts

// This class represents the View that appears when the user approaches a machine
class NearMachineTVC: UITableViewController, ESTBeaconManagerDelegate, UITextFieldDelegate {
    
    // TextFields for the exercise information
    var weightTF : UITextField!
    var repsTF : UITextField!
    var setsTF : UITextField!
    var restTimeTF : UITextField!
    var seatingPositionTF : UITextField!
    
    var managedObjectContext : NSManagedObjectContext!
    
    var machines : [Machine] = []
    var selectedMachines : [Machine] = []
    var selectedExercises : [Exercise] = []
    var nearestBeacon : Machine!
    var beacons:[CLBeacon] = []
    var closestBeacon:CLBeacon!
    
    var machine : Machine!
    //var nearestMachine : CLBeacon!
    let beaconManager = ESTBeaconManager()
    var beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "Beacons")
    //var beaconRegion : CLBeaconRegion!
    var adminForSelectedGym = ""
    
    @IBOutlet var exerciesTV: UITableView!
    
    var activeText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        self.beaconManager.delegate = self
        self.beaconManager.avoidUnknownStateBeacons = true
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard"))
        tapGesture.cancelsTouchesInView = true
        tableView.addGestureRecognizer(tapGesture)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillShow:"),
            name: UIKeyboardWillShowNotification,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillHide:"),
            name: UIKeyboardWillHideNotification,
            object: nil)
        
        self.beaconManager.startRangingBeaconsInRegion(beaconRegion)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.adminForSelectedGym = (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym
        
        // Fetching machines corresponding to the gym selected by the user
        var fetch = NSFetchRequest(entityName: "Gym")
        fetch.predicate = NSPredicate(format: "gymOwner = %@", adminForSelectedGym)
        self.navigationItem.title = (try! self.managedObjectContext.executeFetchRequest(fetch) as! [Gym])[0].gymName
        
        let fetchRequest = NSFetchRequest(entityName: "BeaconDetailsAddedFromParse")
        fetchRequest.predicate = NSPredicate(format: "gymName = %@", (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym)
        
        if ((try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [BeaconDetailsAddedFromParse]).count == 0 {
            self.fetchBeaconDetailsFromParse()
        } else {
            selectedMachines = [Machine]()
            let fetchRequest = NSFetchRequest(entityName: "Machine")
            fetchRequest.predicate = NSPredicate(format: "machineSelected = %@", true)
            for machine in (try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [Machine] {
                if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                    selectedMachines.append(machine)
                    self.tableView.reloadData()
                }
            }
        }
        
        self.deleteDataNotThereInParse()
        
        var beaconsCount : Int32 = 0
        fetch = NSFetchRequest(entityName: "Beacon")
        for beacon in (try! self.managedObjectContext.executeFetchRequest(fetch)) as! [Beacon] {
            if beacon.belongsTo.belongsTo.gymOwner == self.adminForSelectedGym {
                beaconsCount++
            }
        }
        
        let query = PFQuery(className: "BeaconDetails")
        query.whereKey("username", equalTo:self.adminForSelectedGym)
        query.countObjectsInBackgroundWithBlock {
            (count: Int32, error: NSError?) -> Void in
            if error == nil {
                if beaconsCount != count {
                    self.updateBeaconDetailsFromParse()
                }
            }
        }
        
        if selectedMachines.count > 0 {
            if selectedMachines[0].hasBeacon.uuid != ""  {
                self.beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: selectedMachines[0].hasBeacon.uuid)!, identifier: "Beacons")
                self.beaconManager.startRangingBeaconsInRegion(beaconRegion)
            }
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 36/255, green: 73/255, blue: 134/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.beaconManager.stopRangingBeaconsInRegion(beaconRegion)
        self.nearestBeacon = nil
        self.selectedExercises = []
        self.selectedMachines = []
        self.closestBeacon = nil
    }
    
    
    // Invoked when the admin makes any changes to his gym(adding new beacons)
    func fetchBeaconDetailsFromParse() {
        
        let fetchRequest = NSFetchRequest(entityName: "Machine")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "machineName", ascending: true)]
        
        for machine in  (try! managedObjectContext.executeFetchRequest(fetchRequest)) as! [Machine] {
            if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                self.machines.append(machine)
                
                let query = PFQuery(className: "BeaconDetails")
                query.whereKey("username", equalTo: (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym)
                query.whereKey("equipmentName", equalTo: machine.machineName)
                
                query.getFirstObjectInBackgroundWithBlock({ (beaconDetails : PFObject?, error : NSError?) -> Void in
                    if error == nil {
                        if beaconDetails != nil {
                            let beaconEntityDescriptor =  NSEntityDescription.entityForName("Beacon", inManagedObjectContext: self.managedObjectContext)
                            let beacon = NSManagedObject(entity: beaconEntityDescriptor!, insertIntoManagedObjectContext:self.managedObjectContext) as! FitnessTrackerIOS.Beacon
                            beacon.uuid = beaconDetails!.valueForKey("uuid") as! String
                            
                            self.beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: beacon.uuid)!, identifier: "Beacons")
                            
                            beacon.minorID = (NSString(string: beaconDetails!.valueForKey("minorID") as! String)).integerValue
                            beacon.majorID = (NSString(string: beaconDetails!.valueForKey("majorID") as! String)).integerValue
                            beacon.belongsTo = machine
                            do {
                                try self.managedObjectContext.save()
                               } catch _ {
                            }
                            
                            if machine.machineSelected == true {
                                self.selectedMachines.append(machine)
                                }
                            
                        }
                    }
                })
                
            }
        }
        
        let beaconDetailsAddedFromParseEntityDescriptor = NSEntityDescription.entityForName("BeaconDetailsAddedFromParse", inManagedObjectContext: self.managedObjectContext)
        let beaconDetailsAddedFromParse = NSManagedObject(entity: beaconDetailsAddedFromParseEntityDescriptor!, insertIntoManagedObjectContext: self.managedObjectContext) as! FitnessTrackerIOS.BeaconDetailsAddedFromParse
        beaconDetailsAddedFromParse.gymName = (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym
        beaconDetailsAddedFromParse.beaconDetailsAdded = true
        do {
            try self.managedObjectContext.save()
        } catch _ {
        }
    }
    
    // Invoked when the admin makes any changes to his gym(modifying beacons)
    func updateBeaconDetailsFromParse() {
        
        let query = PFQuery(className: "BeaconDetails")
        query.whereKey("username", equalTo:self.adminForSelectedGym)
        query.findObjectsInBackgroundWithBlock {
            (beaconDetails: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let beaconDetails = beaconDetails {
                    for beaconDetail in beaconDetails {
                        var beaconCount = 0
                        let fetchRequest = NSFetchRequest(entityName: "Beacon")
                        fetchRequest.predicate = NSPredicate(format: "minorID = %@", beaconDetail.valueForKey("minorID") as! String)
                        let beacons = (try! self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Beacon])
                        for beacon in beacons {
                            if beacon.belongsTo.belongsTo.gymOwner == self.adminForSelectedGym {
                                beaconCount++
                            }
                        }
                        
                        if beaconCount == 0 {
                            let beaconEntityDescriptor =  NSEntityDescription.entityForName("Beacon", inManagedObjectContext: self.managedObjectContext)
                            let beacon = NSManagedObject(entity: beaconEntityDescriptor!, insertIntoManagedObjectContext:self.managedObjectContext) as! FitnessTrackerIOS.Beacon
                            beacon.uuid = beaconDetail.valueForKey("uuid") as! String
                            beacon.minorID = (NSString(string: beaconDetail.valueForKey("minorID") as! String)).integerValue
                            beacon.majorID = (NSString(string: beaconDetail.valueForKey("majorID") as! String)).integerValue
                            let fetchRequest = NSFetchRequest(entityName: "Machine")
                            fetchRequest.predicate = NSPredicate(format: "machineName = %@", beaconDetail.valueForKey("equipmentName") as! String)
                            for machine in (try! self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Machine]) {
                                if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                                    beacon.belongsTo = machine
                                }
                            }
                            var err : NSError?
                            do {
                                try self.managedObjectContext.save()
                               } catch let error as NSError {
                                err = error
                                print(err)
                            } catch {
                                fatalError()
                            }
                            
                        }
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    // Invoked when the admin makes any changes to his gym(deleting beacons)
    func deleteDataNotThereInParse() {
        let fetchRequest = NSFetchRequest(entityName: "Beacon")
        for beacon in (try! self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Beacon]) {
            if beacon.belongsTo.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                let query = PFQuery(className: "BeaconDetails")
                query.whereKey("uuid", equalTo:beacon.uuid)
                query.whereKey("majorID", equalTo:beacon.majorID.description)
                query.whereKey("minorID", equalTo:beacon.minorID.description)
                query.whereKey("username", equalTo:self.adminForSelectedGym)
                query.whereKey("equipmentName", equalTo:beacon.belongsTo.machineName)
                query.countObjectsInBackgroundWithBlock {
                    (count: Int32, error: NSError?) -> Void in
                    if error == nil {
                        if count == 0 {
                            self.managedObjectContext.deleteObject(beacon)
                            self.updateBeaconDetailsFromParse()
                            do {
                                try self.managedObjectContext.save()
                            } catch {
                                fatalError()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func hideKeyboard() {
        tableView.endEditing(true)
    }

    func keyboardWillShow(note: NSNotification) {
        if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            var frame = tableView.frame
            if frame.size.height > 455 {
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationBeginsFromCurrentState(true)
                UIView.setAnimationDuration(0.1)
                frame.size.height -= keyboardSize.height
                
                tableView.frame = frame
                if activeText != nil {
                    let rect = tableView.convertRect(activeText.bounds, fromView: activeText)
                    tableView.scrollRectToVisible(rect, animated: false)
                }
                UIView.commitAnimations()
            }
        }
    }
    
    func keyboardWillHide(note: NSNotification) {
        if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            var frame = tableView.frame
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(0.1)
            frame.size.height += keyboardSize.height
            tableView.frame = frame
            UIView.commitAnimations()
        }
    }
    
    // Method to retrieve all the exercises that are enabled by the user
    func retrieveSelectedExercises(minorID : NSNumber) -> [Exercise] {
        
        var fetchRequest = NSFetchRequest(entityName: "Beacon")
        for beacon in (try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [Beacon] {
            if beacon.minorID == minorID {
                if beacon.belongsTo.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                    }
            }
        }
        
        var selectedExercises : [Exercise] = []
        
        fetchRequest = NSFetchRequest(entityName: "Beacon")
        fetchRequest.predicate = NSPredicate(format: "minorID = %@", minorID)
        let iBeacons = (try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [Beacon]
        for iBeacon in iBeacons {
            
            if iBeacon.belongsTo.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                let nearestBeacon = iBeacon
                
                // Finding the machine associated with the nearest beacon
                let machine = nearestBeacon.belongsTo as Machine
                
                if machine.machineSelected == true {
                    self.nearestBeacon = machine
                    
                    let exercises = machine.hasExercise.allObjects as! [Exercise]
                    for exercise in exercises {
                        // Finding the exercises associated with this nearest machine
                        if exercise.exerciseSelected == true {
                            if exercise.belongsToMachine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                                selectedExercises.append(exercise)
                            }
                        }
                    }
                    
                }
                
            }
            
        }
        
        // Finding the nearest beacon
        return selectedExercises.sort{$0.exerciseName < $1.exerciseName}
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedExercises.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.nearestBeacon != nil {
            return self.nearestBeacon.machineName
        } else {
            return ""
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("beacon_cell", forIndexPath: indexPath)
        
        let nameLBL = cell.viewWithTag(100) as! UILabel
        nameLBL.font = UIFont(name: "HelveticaNeue-Bold", size: 18.0)
        nameLBL.text = selectedExercises[indexPath.row].exerciseName
        
        // Setting the values of textfields
        
        weightTF = cell.viewWithTag(200) as! UITextField
        if selectedExercises[indexPath.row].weight == -1 {
            weightTF.text = ""
        }else {
            weightTF.text = selectedExercises[indexPath.row].weight.description
        }
        
        
        repsTF = cell.viewWithTag(300) as! UITextField
        if selectedExercises[indexPath.row].numOfReps == -1 {
            repsTF.text = ""
        } else {
            repsTF.text = selectedExercises[indexPath.row].numOfReps.description
        }
        
        setsTF = cell.viewWithTag(400) as! UITextField
        if selectedExercises[indexPath.row].numOfSets == -1 {
            setsTF.text = ""
        } else {
            setsTF.text = selectedExercises[indexPath.row].numOfSets.description
        }
        
        restTimeTF = cell.viewWithTag(500) as! UITextField
        if selectedExercises[indexPath.row].restTime == -1 {
            restTimeTF.text = ""
        } else {
            restTimeTF.text = selectedExercises[indexPath.row].restTime.description
        }
        
        seatingPositionTF = cell.viewWithTag(600) as! UITextField
        if selectedExercises[indexPath.row].seatingPosition == -1 {
            seatingPositionTF.text = ""
        } else {
            seatingPositionTF.text = selectedExercises[indexPath.row].seatingPosition.description
        }
        
        
        weightTF.enabled = false
        repsTF.enabled = false
        setsTF.enabled = false
        restTimeTF.enabled = false
        seatingPositionTF.enabled = false
        
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    // Mark: - ESTBeaconManagerDelegate
    
    // Detecting the beacons as the user moves near a machine
    func beaconManager(manager: AnyObject!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion) {
        
        if (UIApplication.sharedApplication().delegate as! AppDelegate).isDone  == false {
            
        } else {
            self.beacons = beacons as! [CLBeacon]
            
            // If any beacons were detected
            if beacons.count > 0 {
                if selectedMachines.count > 0 {
                    
                    // Identifying closest beacon
                    if closestBeacon == nil && self.beacons.count >= 1 {
                        closestBeacon = self.beacons[0]
                        self.selectedExercises = retrieveSelectedExercises(closestBeacon.minor)
                        if self.selectedExercises.count > 0 {
                            
                            // If the machine has been selected by the user before
                            if nearestBeacon.machineSelected == true {
                                self.tableView.reloadData()
                                
                            }
                        }
                    }
                    
                    // Retrieving the exercises of the closest machine(based on closest beacon)
                    if closestBeacon.major != self.beacons[0].major ||  closestBeacon.minor != self.beacons[0].minor{
                        closestBeacon = self.beacons[0]
                        self.selectedExercises = retrieveSelectedExercises(closestBeacon.minor)
                        
                        if self.selectedExercises.count > 0 {
                            
                            // If the machine has been selected by the user before
                            if nearestBeacon.machineSelected == true {
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }

    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
