//
//  AppLaunch.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/23/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import Foundation
import CoreData

//  This class identifies whether the app is being launched for the first time, by the user
//  We display a tutorial if the app is being launched for the first time
class AppLaunch: NSManagedObject {

    @NSManaged var isFirstLaunch: NSNumber // Boolean in CoreData ends up as NSNumber here

}
