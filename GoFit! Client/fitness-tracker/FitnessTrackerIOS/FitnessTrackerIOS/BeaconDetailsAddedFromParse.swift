//
//  BeaconDetailsAddedFromParse.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/23/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import Foundation
import CoreData

class BeaconDetailsAddedFromParse: NSManagedObject {

    @NSManaged var beaconDetailsAdded: NSNumber
    @NSManaged var gymName: String

}
