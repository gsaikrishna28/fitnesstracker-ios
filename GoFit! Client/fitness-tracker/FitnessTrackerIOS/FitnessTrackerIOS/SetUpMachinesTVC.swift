//
//  SetUpTableViewController.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 7/4/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData
import Parse
import Bolts

// This class is used for enabling the user to select his preferred machines
class SetUpMachinesTVC: UITableViewController, UISearchResultsUpdating {
    
    var managedObjectContext : NSManagedObjectContext!
    var machines : [Machine] = []
    var searchFilteredMachineNames : [Machine] = []
    var resultSearchController = UISearchController()
    var adminForSelectedGym : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        // The user can also search for machines by typing in the search bar
        self.resultSearchController = ({
            
            if let superView = resultSearchController.view.superview
            {
                superView.removeFromSuperview()
            }
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchResultsUpdater = self
            controller.searchBar.searchBarStyle = UISearchBarStyle.Minimal
            controller.searchBar.barTintColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
            controller.searchBar.tintColor = UIColor(red: 42/255, green: 82/255, blue: 139/255, alpha: 1)
            controller.searchBar.placeholder = "Search Machines"
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 36/255, green: 73/255, blue: 134/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        adminForSelectedGym = (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym
        
        // Fetching machines list from CoreData
        var fetch = NSFetchRequest(entityName: "Machine")
        fetch.sortDescriptors = [NSSortDescriptor(key: "machineName", ascending: true)]
        for machine in ((try! self.managedObjectContext.executeFetchRequest(fetch)) as! [Machine]) {
            if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                machines.append(machine)
            }
        }
        
        self.deleteDataNotThereInParse()
        
        var machineCount : Int32 = 0
        fetch = NSFetchRequest(entityName: "Machine")
        for machine in (try! self.managedObjectContext.executeFetchRequest(fetch)) as! [Machine] {
            if machine.belongsTo.gymOwner == adminForSelectedGym {
                machineCount++
            }
        }
        
                    self.updateDataFromParse()
        
        self.tableView.reloadData()
    }
    
    // Invoked when the admin makes any changes to his gym(adding machines)
    func updateDataFromParse() {
        
        let fetchRequest = NSFetchRequest(entityName: "Gym")
        fetchRequest.predicate = NSPredicate(format: "gymOwner = %@", self.adminForSelectedGym)
        let gym = ((try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [Gym])[0]
        
        let query = PFQuery(className: "GymEquipments")
        query.whereKey("username", equalTo:self.adminForSelectedGym)
        query.findObjectsInBackgroundWithBlock {
            (gymEquipments: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let gymEquipments = gymEquipments {
                    for gymEquipment in gymEquipments {
                        var machineCount = 0
                        let fetchRequest = NSFetchRequest(entityName: "Machine")
                        fetchRequest.predicate = NSPredicate(format: "machineName = %@", gymEquipment.valueForKey("equipmentName") as! String)
                        let machines = (try! self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Machine])
                        for machine in machines {
                            if machine.belongsTo.gymOwner == self.adminForSelectedGym {
                                machineCount++
                            }
                        }
                        
                        if machineCount == 0 {
                            let machineEntityDescriptor =  NSEntityDescription.entityForName("Machine", inManagedObjectContext: self.managedObjectContext)
                            let machine = NSManagedObject(entity: machineEntityDescriptor!, insertIntoManagedObjectContext:self.managedObjectContext) as! FitnessTrackerIOS.Machine
                            machine.machineName = gymEquipment.valueForKey("equipmentName") as! String
                            machine.machineID = gymEquipment.objectId!
                            machine.machineSelected = false
                            machine.belongsTo = gym
                            var machines = gym.hasMacines.allObjects as! [Machine]
                            machines.append(machine)
                            gym.setValue(NSSet(array:machines) , forKey: "hasMacines")
                            self.machines.append(machine)
                            self.tableView.reloadData()
                            var err : NSError?
                            do {
                                try self.managedObjectContext.save()
                            } catch let error as NSError {
                                err = error
                                print(err)
                            } catch {
                                fatalError()
                            }
                            
                        }
                        
                        
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    // Invoked when the admin makes any changes to his gym(deleting machines)
    func deleteDataNotThereInParse() {
        
        let fetchRequest = NSFetchRequest(entityName: "Machine")
        for machine in (try! self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Machine]) {
            if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                let query = PFQuery(className: "GymEquipments")
                query.whereKey("equipmentName", equalTo:machine.machineName)
                query.whereKey("username", equalTo:self.adminForSelectedGym)
                query.countObjectsInBackgroundWithBlock {
                    (count: Int32, error: NSError?) -> Void in
                    if error == nil {
                        if count == 0 {
                            self.managedObjectContext.deleteObject(machine)
                            self.machines.removeAtIndex(self.machines.indexOf(machine)!)
                            self.tableView.reloadData()
                            do {
                                try self.managedObjectContext.save()
                            } catch {
                                fatalError()
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    deinit{
        if let superView = resultSearchController.view.superview
        {
            superView.removeFromSuperview()
        }
    }
    
    // Called when the user clicks done button, after selecting the machines
    @IBAction func done(sender: AnyObject) {
        self.dismissViewControllerAnimated(true , completion: nil )
    }
    
    
    // Called when the user doesn't want to save changes, after selecting the machines
    @IBAction func cancel(sender: AnyObject) {
        
        if let superView = resultSearchController.view.superview
        {
            superView.removeFromSuperview()
        }
        
        self.dismissViewControllerAnimated(true , completion: nil )
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.resultSearchController.active) {
            return self.searchFilteredMachineNames.count
        }
        else {
            return self.machines.count
        }
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Machines"
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("exercise_cell", forIndexPath: indexPath) 
        
        // Allowing the user to select a machine(indicated by checkmark)
        if (self.resultSearchController.active) {
            cell.textLabel?.text = searchFilteredMachineNames[indexPath.row].machineName
            
            if searchFilteredMachineNames[indexPath.row].machineSelected == true {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
            
        }
        else {
            cell.textLabel?.text = machines[indexPath.row].machineName
            
            if machines[indexPath.row].machineSelected == true {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        }
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        let data = cell.textLabel?.text
        
        let fetchRequest = NSFetchRequest(entityName:"Machine")
        fetchRequest.predicate = NSPredicate(format: "machineName=%@", data!)
        //var error : NSError?
        var machines = (try! managedObjectContext.executeFetchRequest(fetchRequest)) as! [Machine]
        
        var requiredIndex = 0
        for var i = 0; i < machines.count; i++ {
            if machines[i].belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                requiredIndex = i
            }
        }
        
        if cell.accessoryType == UITableViewCellAccessoryType.None {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            machines[requiredIndex].setValue(true , forKey: "machineSelected")
            do {
                try managedObjectContext.save()
            } catch _ {
            }
            //machineNames[indexPath.row].machineSelected = true
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
            machines[requiredIndex].setValue(false , forKey: "machineSelected")
            do {
                try managedObjectContext.save()
            } catch _ {
            }
            //machineNames[indexPath.row].machineSelected = false
        }
        
    }
    
    // MARK: - UISearchResultsUpdating
    
    // Allows the user to search for machines using the search bar
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        searchFilteredMachineNames.removeAll(keepCapacity: false)
        
        let searchPredicate = NSPredicate(format: "machineName CONTAINS[c] %@", searchController.searchBar.text!)
        
        let searchMachines = (machines as NSArray).filteredArrayUsingPredicate(searchPredicate)
        searchFilteredMachineNames = searchMachines as! [Machine]
        
        self.tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}