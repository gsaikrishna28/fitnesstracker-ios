//
//  SubExerciseTableViewCell.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 6/30/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData

// This class represents a cell in ExerciseTVC
class ExerciseTVCell: UITableViewCell{
    
    
    var managedObjectContext : NSManagedObjectContext!
    
    var parentMachine : Machine!
    var exercises : [Exercise] = []
    
    
    // Toggle button for the exercise
    @IBOutlet weak var exerciseSelected: UISwitch!
    
    
    // TextFields for exercise information
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var numOfRepsTF: UITextField!
    @IBOutlet weak var seatingPositionTF: UITextField!
    @IBOutlet weak var numOfSetsTF: UITextField!
    @IBOutlet weak var restTimeTF: UITextField!
    
    
    // Labels for each TextField
    @IBOutlet weak var exerciseNameLBL: UILabel!
    @IBOutlet weak var weightLBL: UILabel!
    @IBOutlet weak var numOfRepsLBL: UILabel!
    @IBOutlet weak var seatingPositionLBL: UILabel!
    @IBOutlet weak var numOfSetsLBL: UILabel!
    @IBOutlet weak var restTimeLBL: UILabel!
    
    
    // Fetching exercise information(which user enables) to populate the cell contents
    func fetchCellData(textLBL : String) -> [Exercise]{
        
        managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        parentMachine = (UIApplication.sharedApplication().delegate as! AppDelegate).selectedMachine
        exercises = parentMachine.hasExercise.allObjects as! [Exercise]
        
        let fetchRequest : NSFetchRequest = NSFetchRequest(entityName: "Exercise")
        fetchRequest.predicate = NSPredicate(format: "exerciseName = %@", textLBL)
        //var error : NSError?
        
        var exercisesToReturn = [Exercise]()
        
        for exercise in (try! managedObjectContext.executeFetchRequest(fetchRequest)) as! [Exercise] {
            if exercise.belongsToMachine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                exercisesToReturn.append(exercise)
            }
        }
        
        return exercisesToReturn
        
    }
    
    // Updating the textfield
    @IBAction func weightTFTap(sender: AnyObject) {
        
        var fetchedSubExercises = fetchCellData(exerciseNameLBL.text!)
        
        if weightTF.text == ""{
            fetchedSubExercises[0].weight = NSDecimalNumber(string: "-1")
        }else {
            fetchedSubExercises[0].weight = NSDecimalNumber(string: weightTF.text)
        }
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
        
    }
    
    // Updating the textfield
    @IBAction func repsTFTap(sender: AnyObject) {
        
        var fetchedSubExercises = fetchCellData(exerciseNameLBL.text!)
        
        if numOfRepsTF.text == ""{
            fetchedSubExercises[0].numOfReps = NSNumber(int: NSString(string: "-1").intValue)
        }else {
            fetchedSubExercises[0].numOfReps = NSNumber(int: NSString(string: numOfRepsTF.text!).intValue)
        }
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    // Updating the textfield
    @IBAction func seatingPositionTFTap(sender: AnyObject) {
        
        var fetchedSubExercises = fetchCellData(exerciseNameLBL.text!)
        
        if seatingPositionTF.text == "" {
            fetchedSubExercises[0].seatingPosition = NSNumber(int: NSString(string: "-1").intValue)
        }else {
            fetchedSubExercises[0].seatingPosition = NSNumber(int: NSString(string: seatingPositionTF.text!).intValue)
        }
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    // Updating the textfield
    @IBAction func setsTFTap(sender: AnyObject) {
        var fetchedSubExercises = fetchCellData(exerciseNameLBL.text!)
        
        if numOfSetsTF.text == "" {
            fetchedSubExercises[0].numOfSets = NSNumber(int: NSString(string: "-1").intValue)
        } else {
            fetchedSubExercises[0].numOfSets = NSNumber(int: NSString(string: numOfSetsTF.text!).intValue)
        }
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
        
    }
    
    // Updating the textfield
    @IBAction func timeTFTap(sender: AnyObject) {
         var fetchedSubExercises = fetchCellData(exerciseNameLBL.text!)
        
        if restTimeTF.text == "" {
            fetchedSubExercises[0].restTime = NSDecimalNumber(string: "-1")
        } else {
            fetchedSubExercises[0].restTime = NSDecimalNumber(string: restTimeTF.text)
        }
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    
    @IBAction func switchTap(sender: AnyObject) {
        var fetchedSubExercises = fetchCellData(exerciseNameLBL.text!)
        
        if exerciseSelected.on {
            fetchedSubExercises[0].setValue(true , forKey: "exerciseSelected")
            weightTF.hidden = false
            numOfRepsTF.hidden = false
            seatingPositionTF.hidden = false
            numOfSetsTF.hidden = false
            restTimeTF.hidden = false
            weightLBL.hidden = false
            numOfRepsLBL.hidden = false
            seatingPositionLBL.hidden = false
            numOfSetsLBL.hidden = false
            restTimeLBL.hidden = false
        }
        else{
            fetchedSubExercises[0].setValue(false, forKey: "exerciseSelected")
            weightTF.hidden = true
            numOfRepsTF.hidden = true
            seatingPositionTF.hidden = true
            numOfSetsTF.hidden = true
            restTimeTF.hidden = true
            weightLBL.hidden = true
            numOfRepsLBL.hidden = true
            seatingPositionLBL.hidden = true
            numOfSetsLBL.hidden = true
            restTimeLBL.hidden = true
        }
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
