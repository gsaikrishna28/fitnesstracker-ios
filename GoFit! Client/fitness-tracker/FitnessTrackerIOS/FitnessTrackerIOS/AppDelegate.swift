//
//  AppDelegate.swift
//  FitnessTrackerIOS
//
//  Created by dinesh karem on 6/25/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData
import Parse
import Bolts


// Beacon monitoring starts with the execution of this class
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, ESTBeaconManagerDelegate {
    
    var window: UIWindow?
    
    // Shared variables accessed throughout the program
    var machineRow : Int = 0
    var selectedMachine : Machine!
    var selectedExercises : [Exercise] = []
    var nearestBeacon : CLBeacon!
    var adminForSelectedGym : String = ""
    var viewDidLoadFlag = false
    var isDone : Bool = true
    
    // Instantiating a property that holds the beacon manager
    let beaconManager = ESTBeaconManager()
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Instantiating a PageController for the tutorial
        // The RGB values indicate how much color of Red, Green, Blue is included
        // Each color is formed by a mix of RGB ratios
        // For detailed description about RGB values refer to https://en.wikipedia.org/wiki/RGB_color_model
        let pageController = UIPageControl.appearance()
        pageController.pageIndicatorTintColor = UIColor.whiteColor()
        pageController.currentPageIndicatorTintColor = UIColor.blackColor()
        pageController.backgroundColor = UIColor(red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1)
        let moc = self.managedObjectContext
        
        
        // Fetching data from AppLaunch entity to check if the app is being opened for the first time
        var fetch : NSFetchRequest = NSFetchRequest(entityName: "AppLaunch")
        let appLaunchCount = ((try! moc!.executeFetchRequest(fetch)) as! [AppLaunch]).count
        
        if appLaunchCount >= 1 {
            
            fetch = NSFetchRequest(entityName: "PreviouslyEnteredGym")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let gyms = ((try! moc!.executeFetchRequest(fetch)) as! [PreviouslyEnteredGym])
            
            // To check if the user has already selected a gym(using zipcode)
            if gyms.count > 0 {
                self.adminForSelectedGym = gyms[0].gymOwner!
                self.window?.rootViewController = storyboard.instantiateViewControllerWithIdentifier("tabBarController") as! UITabBarController
                
            } else {
                
                self.window?.rootViewController = storyboard.instantiateViewControllerWithIdentifier("searchingGymsNVC") as! UINavigationController
                
            }
            
        }
        
        self.window?.makeKeyAndVisible()
        
        // Beacon monitoring starts here
        // Estimote beacons ships with a default UUID
        self.beaconManager.delegate = self
        self.beaconManager.requestAlwaysAuthorization()
        self.beaconManager.startMonitoringForRegion(CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "Estimotes"))
        
        // Registering user notification settings
        UIApplication.sharedApplication().registerUserNotificationSettings(
            UIUserNotificationSettings(forTypes: .Alert, categories: nil))
        
        // Getting access to parse application
        Parse.enableLocalDatastore()
        Parse.setApplicationId("N6Xd2UIrKvStvASquZmKdwUoYd9BVr92INkscnVP", clientKey: "2HxwbqJF6teLL8NQQvopUEGi9tRxyOt45Qm1RXOu")
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        return true
    }
    
    
    // Method that sends a notification when the user enters the gym
    func beaconManager(manager: AnyObject, didEnterRegion region: CLBeaconRegion) {
        let notification = UILocalNotification()
        notification.alertBody =
        "Welcome to GoFit"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "nwmissouri.edu.Fitness_Tracker" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("FitnessTrackerIOS", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("FitnessTrackerIOS.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error!.userInfo)")
                    abort()
                }
            }
        }
    }
    
    
    
}

