//
//  BeaconTableTableViewCell.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 7/15/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData

// This class represents a cell in NearMachineTVC
class NearMachineTVCell: UITableViewCell, ESTBeaconManagerDelegate {
    
    var managedObjectContext : NSManagedObjectContext!
    
    
    // Variables associated for each cell
    @IBOutlet weak var exerciseNameLBL: UILabel!
    @IBOutlet weak var editBTN: UIButton!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var numOfSetsTF: UITextField!
    @IBOutlet weak var numOfRepsTF: UITextField!
    @IBOutlet weak var restTimeTF: UITextField!
    @IBOutlet weak var seatingPositionTF: UITextField!
    
    
    // Called when the user clicks on "Edit" button while working out
    @IBAction func tapEdit(sender: AnyObject) {
        
        if editBTN.titleLabel?.text == "Edit" {
            
            let story = UIStoryboard(name: "Main", bundle: nil)
            (story.instantiateViewControllerWithIdentifier("nearMachineTVC") as! NearMachineTVC).beaconManager.stopMonitoringForRegion((story.instantiateViewControllerWithIdentifier("nearMachineTVC") as! NearMachineTVC).beaconRegion)
            
            let beaconManager = ESTBeaconManager()
            beaconManager.delegate = self
            beaconManager.avoidUnknownStateBeacons = true
            
            editBTN.setTitle("Done", forState: UIControlState.Normal)
            (UIApplication.sharedApplication().delegate as! AppDelegate).isDone = false
            
            weightTF.enabled = true
            numOfRepsTF.enabled = true
            numOfSetsTF.enabled = true
            restTimeTF.enabled = true
            seatingPositionTF.enabled = true
            
            weightTF.borderStyle = UITextBorderStyle.RoundedRect
            numOfRepsTF.borderStyle = UITextBorderStyle.RoundedRect
            numOfSetsTF.borderStyle = UITextBorderStyle.RoundedRect
            restTimeTF.borderStyle = UITextBorderStyle.RoundedRect
            seatingPositionTF.borderStyle = UITextBorderStyle.RoundedRect
            
        }
        else if editBTN.titleLabel?.text == "Done" {
            
            
            let beaconManager = ESTBeaconManager()
            let beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "Estimotes")
            beaconManager.delegate = self
            beaconManager.avoidUnknownStateBeacons = true
            beaconManager.startRangingBeaconsInRegion(beaconRegion)
            
            editBTN.setTitle("Edit", forState: UIControlState.Normal)
            (UIApplication.sharedApplication().delegate as! AppDelegate).isDone = true
            
            weightTF.enabled = false
            numOfRepsTF.enabled = false
            numOfSetsTF.enabled = false
            restTimeTF.enabled = false
            seatingPositionTF.enabled = false
            
            weightTF.borderStyle = UITextBorderStyle.None
            numOfRepsTF.borderStyle = UITextBorderStyle.None
            numOfSetsTF.borderStyle = UITextBorderStyle.None
            restTimeTF.borderStyle = UITextBorderStyle.None
            seatingPositionTF.borderStyle = UITextBorderStyle.None
            
        }
    }
   
    
    // Fecthing exercise information from CoreData
    func fetchCellData(textLBL : String) -> [Exercise]{
        
        managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let fetchRequest : NSFetchRequest = NSFetchRequest(entityName: "Exercise")
        fetchRequest.predicate = NSPredicate(format: "exerciseName = %@", textLBL)
        
        var exercisesToReturn = [Exercise]()
        
        
        for exercise in (try! managedObjectContext.executeFetchRequest(fetchRequest)) as! [Exercise] {
            if exercise.belongsToMachine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                exercisesToReturn.append(exercise)
            }
        }
        
        return exercisesToReturn
        
    }
    
    
    // Invoked if the user edits the textfield while workingout
    @IBAction func weigthTF(sender: AnyObject) {
        
        var subEx = fetchCellData(exerciseNameLBL.text!)
        
        let weight = NSDecimalNumber(string: weightTF.text)
        subEx[0].setValue(weight , forKey: "weight")
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    // Invoked if the user edits the textfield while workingout
    @IBAction func repsTF(sender: AnyObject) {
        
        var subEx = fetchCellData(exerciseNameLBL.text!)
        
        let reps = NSNumber(integer: NSString(string: numOfRepsTF.text!).integerValue)
        subEx[0].setValue(reps , forKey: "numOfReps")
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    // Invoked if the user edits the textfield while workingout
    @IBAction func setsTF(sender: AnyObject) {
        
        var subEx = fetchCellData(exerciseNameLBL.text!)
        
        let sets = NSNumber(integer: NSString(string: numOfSetsTF.text!).integerValue)
        subEx[0].setValue(sets , forKey: "numOfSets")
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    // Invoked if the user edits the textfield while workingout
    @IBAction func restTimeTF(sender: AnyObject) {
        
        var subEx = fetchCellData(exerciseNameLBL.text!)
        
        let restTime = NSDecimalNumber(string: restTimeTF.text)
        subEx[0].setValue(restTime , forKey: "restTime")
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
        
    }
    
    // Invoked if the user edits the textfield while workingout
    @IBAction func seatingPositionTF(sender: AnyObject) {
        
        var subEx = fetchCellData(exerciseNameLBL.text!)
        
        let seatingPosition = NSNumber(integer: NSString(string: seatingPositionTF.text!).integerValue)
        subEx[0].setValue(seatingPosition , forKey: "seatingPosition")
        
        do {
            try managedObjectContext.save()
        } catch _ {
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
