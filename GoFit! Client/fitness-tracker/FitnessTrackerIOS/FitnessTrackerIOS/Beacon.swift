//
//  Beacon.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/23/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import Foundation
import CoreData

//  This class represents the information that is stored by a beacon
class Beacon: NSManagedObject {

    @NSManaged var majorID: NSNumber        // Changes for every fitness center
    @NSManaged var minorID: NSNumber        // Changes for every machine within a fitness center
    @NSManaged var uuid: String             // Remains same always as it is used to identify the beacons
    @NSManaged var belongsTo: Machine

}
