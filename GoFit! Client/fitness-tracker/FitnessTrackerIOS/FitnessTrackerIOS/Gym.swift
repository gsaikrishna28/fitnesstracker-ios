//
//  Gym.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/23/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import Foundation
import CoreData

// This class represents a gym
// It shows the different information stored about each gym
class Gym: NSManagedObject {

    @NSManaged var gymAddress: String
    @NSManaged var gymCity: String
    @NSManaged var gymID: String
    @NSManaged var gymName: String
    @NSManaged var gymOwner: String
    @NSManaged var gymState: String
    @NSManaged var gymZipcode: String
    @NSManaged var hasMacines: NSSet

}
