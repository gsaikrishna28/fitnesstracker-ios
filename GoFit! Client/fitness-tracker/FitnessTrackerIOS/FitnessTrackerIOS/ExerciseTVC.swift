//
//  ExerciseTVC.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 7/20/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData
import Parse

// This class represents all the exercises for selected machine
class ExerciseTVC: UITableViewController, UITextFieldDelegate {

    var parentMachine : Machine!
    var exercises : [Exercise]!
    var cellDictionary : [Int:Bool] = [:]
    var managedObjectContext : NSManagedObjectContext!
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    var boxView = UIView()
    
    // TextFields for each exercise
    var weightTF:UITextField!;
    var repsTF:UITextField!;
    var setsTF:UITextField!;
    var timeTF:UITextField!;
    var seatingPositionTF:UITextField!;
    var dataFetchedFromParse = false

    var activeText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let tapGesture = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard"))
        tapGesture.cancelsTouchesInView = true
        tableView.addGestureRecognizer(tapGesture)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillShow:"),
            name: UIKeyboardWillShowNotification,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillHide:"),
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        parentMachine = (UIApplication.sharedApplication().delegate as! AppDelegate).selectedMachine
        exercises = parentMachine.hasExercise.allObjects as! [Exercise]
        exercises.sortInPlace({$0.exerciseName < $1.exerciseName})
        self.navigationItem.title = parentMachine.machineName
        if exercises.count == 0 {
            if dataFetchedFromParse == false {
                fetchUpdatedateDataFromParse()
            }
            
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.dataFetchedFromParse = false
    }
    
    func hideKeyboard() {
        tableView.endEditing(true)
    }
    
    
    // Fethcing exercises information from parse
    func fetchExercisesDataFromParse() {
        self.dataFetchedFromParse = true
        let fetchRequest = NSFetchRequest(entityName: "ExercisesAddedFromParse")
        fetchRequest.predicate = NSPredicate(format: "gymName = %@", (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym)
        if ((try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [ExercisesAddedFromParse]).count == 0 {
            
            // Instantiating and starting activity indicator (like Loading...)
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            var err : NSError?
            let fetchRequest : NSFetchRequest = NSFetchRequest(entityName: "Machine")
            let machines = (try! managedObjectContext.executeFetchRequest(fetchRequest)) as! [Machine]
            for machine in machines {
                if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                    
                    
                    // Fetching the exercises
                    let query = PFQuery(className: "ExerciseList")
                    query.whereKey("equipmentName", equalTo:machine.machineName)
                    query.findObjectsInBackgroundWithBlock {
                        (exercises: [PFObject]?, error: NSError?) -> Void in
                        activityIndicator.startAnimating()
                        if error == nil {
                            if let exercises = exercises {
                                for exercise in exercises {
                                    let subExerciseEntityDescriptor = NSEntityDescription.entityForName("Exercise", inManagedObjectContext: self.managedObjectContext)
                                    let subExercise = NSManagedObject(entity: subExerciseEntityDescriptor!, insertIntoManagedObjectContext: self.managedObjectContext) as! FitnessTrackerIOS.Exercise
                                    subExercise.exerciseName = exercise.valueForKey("exerciseName") as! String
                                    subExercise.weight = -1
                                    subExercise.numOfReps = -1
                                    subExercise.restTime = -1
                                    subExercise.numOfSets = -1
                                    subExercise.seatingPosition = -1
                                    subExercise.belongsToMachine = machine
                                    do {
                                        try self.managedObjectContext.save()
                                    } catch let error as NSError {
                                        err = error
                                        print(err)
                                    } catch {
                                        fatalError()
                                    }
                                    self.parentMachine = (UIApplication.sharedApplication().delegate as! AppDelegate).selectedMachine
                                    self.exercises = self.parentMachine.hasExercise.allObjects as! [Exercise]
                                    self.exercises.sortInPlace({$0.exerciseName < $1.exerciseName})
                                    self.tableView.reloadData()
                                    activityIndicator.stopAnimating()
                                }
                            }
                        } else {
                            // Log details of the failure
                            print("Error: \(error!) \(error!.userInfo)")
                        }
                    }
                }
            }
            activityIndicator.stopAnimating()
            
            let exercisesAddedFromParseEntityDescriptor = NSEntityDescription.entityForName("ExercisesAddedFromParse", inManagedObjectContext: self.managedObjectContext)
            let exercisesAddedFromParse = NSManagedObject(entity: exercisesAddedFromParseEntityDescriptor!, insertIntoManagedObjectContext: self.managedObjectContext) as! FitnessTrackerIOS.ExercisesAddedFromParse
            exercisesAddedFromParse.gymName = (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym
            exercisesAddedFromParse.exerciseAdded = true
            do {
                try self.managedObjectContext.save()
            } catch _ {
            }
        }
        dataFetchedFromParse = false
    }
    
    // Invoked when the admin makes any changes to his gym(adding exercises)
    func fetchUpdatedateDataFromParse() {
        
        let query = PFQuery(className: "ExerciseList")
        query.whereKey("equipmentName", equalTo:self.parentMachine.machineName)
        query.findObjectsInBackgroundWithBlock {
            (exercises: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                if let exercises = exercises {
                    for exercise in exercises {
                        let subExerciseEntityDescriptor = NSEntityDescription.entityForName("Exercise", inManagedObjectContext: self.managedObjectContext)
                        let subExercise = NSManagedObject(entity: subExerciseEntityDescriptor!, insertIntoManagedObjectContext: self.managedObjectContext) as! FitnessTrackerIOS.Exercise
                        subExercise.exerciseName = exercise.valueForKey("exerciseName") as! String
                        subExercise.weight = -1
                        subExercise.numOfReps = -1
                        subExercise.restTime = -1
                        subExercise.numOfSets = -1
                        subExercise.seatingPosition = -1
                        subExercise.belongsToMachine = self.parentMachine
                        do {
                            try self.managedObjectContext.save()
                        } catch let error as NSError {
                            print(error)
                        } catch {
                            fatalError()
                        }
                        self.parentMachine = (UIApplication.sharedApplication().delegate as! AppDelegate).selectedMachine
                        self.exercises = self.parentMachine.hasExercise.allObjects as! [Exercise]
                        self.exercises.sortInPlace({$0.exerciseName < $1.exerciseName})
                        self.tableView.reloadData()
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeText = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        activeText = nil
    }
    
    func keyboardWillShow(note: NSNotification) {
        if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            var frame = tableView.frame
            if frame.size.height > 455 {
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationBeginsFromCurrentState(true)
                UIView.setAnimationDuration(0.1)
                frame.size.height -= keyboardSize.height
                
                tableView.frame = frame
                if activeText != nil {
                    let rect = tableView.convertRect(activeText.bounds, fromView: activeText)
                    tableView.scrollRectToVisible(rect, animated: false)
                }
                UIView.commitAnimations()
            }
        }
    }
    
    func keyboardWillHide(note: NSNotification) {
        if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            var frame = tableView.frame
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(0.1)
            frame.size.height += keyboardSize.height
            tableView.frame = frame
            UIView.commitAnimations()
        }
    }
    
    // Method that is called when the user enables/disables the toggle button
    @IBAction func switchTap(sender: AnyObject) {
        let onSwitch = sender as! UISwitch
        let view = onSwitch.superview!
        let cell = view.superview as! ExerciseTVCell
        let indexPath = self.tableView.indexPathForCell(cell)
        
        if (sender as! UISwitch).on == true {
            cellDictionary[indexPath!.row] = true
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
        else {
            cellDictionary[indexPath!.row] = false
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
 
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if exercises == nil {
            return 0
        }
        return exercises.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Exercises"
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("regime_cell", forIndexPath: indexPath)
        
        // Accessing the cells using tags
        weightTF = cell.viewWithTag(300) as! UITextField
        repsTF = cell.viewWithTag(400) as! UITextField
        seatingPositionTF = cell.viewWithTag(500) as! UITextField
        setsTF = cell.viewWithTag(600) as! UITextField
        timeTF = cell.viewWithTag(700) as! UITextField
        
        
        // Setting all the textfields to empty
        if exercises[indexPath.row].weight == -1
        {
            weightTF.text = ""
        }
        else
        {
            weightTF.text = exercises[indexPath.row].weight.description
        }
        
        
        if exercises[indexPath.row].numOfReps == -1
        {
            repsTF.text = ""
        }
        else
        {
            repsTF.text = exercises[indexPath.row].numOfReps.description
        }
        
        
        if exercises[indexPath.row].seatingPosition == -1
        {
            seatingPositionTF.text = ""
        }
        else
        {
            seatingPositionTF.text = exercises[indexPath.row].seatingPosition.description
        }
        
        
        if exercises[indexPath.row].numOfSets == -1
        {
            setsTF.text = ""
        }
        else
        {
            setsTF.text = exercises[indexPath.row].numOfSets.description
        }
        
        
        if exercises[indexPath.row].restTime == -1
        {
            timeTF.text = ""
        }
        else
        {
            timeTF.text = exercises[indexPath.row].restTime.description
        }
        
        let subExerciseNameLBL : UILabel = cell.viewWithTag(100) as! UILabel
        subExerciseNameLBL.text = exercises[indexPath.row].exerciseName
        subExerciseNameLBL.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)
        
        let exerciseToggleBTN : UISwitch = cell.viewWithTag(200) as! UISwitch
        
        
        // Checking if the toggle button is enabled for a each exercise
        if (exercises[indexPath.row].exerciseSelected == true) {
            cellDictionary[indexPath.row] = true
            weightTF.hidden = false
            repsTF.hidden = false
            seatingPositionTF.hidden = false
            timeTF.hidden = false
            setsTF.hidden = false
            
            (cell.viewWithTag(10) as! UILabel).hidden = false
            (cell.viewWithTag(20) as! UILabel).hidden = false
            (cell.viewWithTag(30) as! UILabel).hidden = false
            (cell.viewWithTag(40) as! UILabel).hidden = false
            (cell.viewWithTag(50) as! UILabel).hidden = false
        }
        else {
            exerciseToggleBTN.on = false
            cellDictionary[indexPath.row] = false
            weightTF.hidden = true
            repsTF.hidden = true
            seatingPositionTF.hidden = true
            timeTF.hidden = true
            setsTF.hidden = true
            
            (cell.viewWithTag(10) as! UILabel).hidden = true
            (cell.viewWithTag(20) as! UILabel).hidden = true
            (cell.viewWithTag(30) as! UILabel).hidden = true
            (cell.viewWithTag(40) as! UILabel).hidden = true
            (cell.viewWithTag(50) as! UILabel).hidden = true
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if cellDictionary[indexPath.row] == true{
            return 175.0
        }
        else {
            return 55.0
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    // MARK: - UITextFeildDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
