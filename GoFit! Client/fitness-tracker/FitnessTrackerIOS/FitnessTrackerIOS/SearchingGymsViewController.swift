//
//  SearchingGymsViewController.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/13/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import Parse
import Bolts
import CoreData

// This class allows user to enter his zipcode and search for gyms located in that zipcode
class SearchingGymsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    var gymNames : [String] = [] // Gym Names
    var gymDictionary : [String : String] = [:] // Key-Value pairs of gym names and assosiated addresses
    var managedObjectContext : NSManagedObjectContext!
    
    @IBOutlet weak var zipCodeTF: UITextField!
    @IBOutlet weak var gymTV: UITableView!
    
    // Regular exoression to allow only valid US/Canada zipcodes
    let zipcodeRegEx: NSRegularExpression = try! NSRegularExpression(pattern: "(^[A-Za-z]\\d[A-Za-z][ ]?\\d[A-Za-z]\\d$)|(^\\d{5}(?:[-\\s]\\d{4})?$)", options: NSRegularExpressionOptions.CaseInsensitive)
    
    func testZipcode(input: String) -> Bool {
        let matches = self.zipcodeRegEx.matchesInString(input, options: NSMatchingOptions.ReportCompletion, range:NSMakeRange(0, input.characters.count))
        return matches.count > 0
    }
    
    // Action that is performed when user taps on "Search Gyms" button
    @IBAction func searchGymsBTN(sender: AnyObject) {
        
        if(!(testZipcode(zipCodeTF.text!))) {
            
            let alert = UIAlertController(title: "", message:"Please enter a valid zipcode", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}

            self.zipCodeTF.text = ""
            self.zipCodeTF.placeholder = "Enter your zipcode"
            return
        }
        
        self.view.endEditing(true)
        gymDictionary = [:]
        gymNames = []
        self.gymTV.reloadData()
        
        // Instantiating and starting activity indicator (like Loading...)
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
       
        // Fetching data from Parse - GymInfo table
        let query = PFQuery(className:"GymInfo")
        // Condition to fetch gyms located in particular zipcode
        query.whereKey("zipcode", equalTo:zipCodeTF.text!)
        query.findObjectsInBackgroundWithBlock {
            (gymObjects: [PFObject]?, error: NSError?) -> Void in
            activityIndicator.startAnimating()
            if error == nil {
                if let gymObjects = gymObjects {
                    if gymObjects.count == 0 {
                        activityIndicator.stopAnimating()
                        let alert = UIAlertController(title: "", message:"No gyms found, please try with other zipcode", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                        self.presentViewController(alert, animated: true){}

                    }
                    
                    // Fetching all gyms information
                    for gymObject in gymObjects {
                        let gymEntityDescriptor =  NSEntityDescription.entityForName("Gym", inManagedObjectContext: self.managedObjectContext)
                        let gym = NSManagedObject(entity: gymEntityDescriptor!, insertIntoManagedObjectContext:self.managedObjectContext) as! FitnessTrackerIOS.Gym
                        gym.gymName = gymObject.valueForKey("gymName") as! String
                        gym.gymID = gymObject.objectId! as String
                        gym.gymAddress = gymObject.valueForKey("address") as! String
                        gym.gymCity = gymObject.valueForKey("city") as! String
                        gym.gymOwner = gymObject.valueForKey("username") as! String
                        gym.gymZipcode = gymObject.valueForKey("zipcode") as! String
                        gym.gymState = gymObject.objectForKey("state") as! String
                        
                        self.gymNames.append(gymObject.valueForKey("gymName") as! String)
                        self.gymDictionary[gymObject.valueForKey("gymName") as! String] = (gymObject.valueForKey("address") as! String) + ", " + (gymObject.valueForKey("city") as! String) + ", " + (gymObject.objectForKey("state") as! String) + " - " + (gymObject.valueForKey("zipcode") as! String)
                        
                        var error : NSError?
                        do {
                            try self.managedObjectContext.save()
                        } catch let error1 as NSError {
                            error = error1
                            print(error)
                        } catch {
                            fatalError()
                        }

                        if gymObjects.count == self.gymDictionary.count {
                            activityIndicator.stopAnimating()
                            self.zipCodeTF.text = ""
                            self.zipCodeTF.placeholder = "Enter your zipcode"
                        }
                    }
                    self.gymNames.sortInPlace{$0 < $1}
                    self.gymTV.reloadData()
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        // Customising navigation controller
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 36/255, green: 73/255, blue: 134/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationItem.title = "GoFit"
        
    }
    
    override func viewWillAppear(animated: Bool) {
        if Reachability.isConnectedToNetwork() == false {
            let alert = UIAlertController(title:"No Internet Connection :(", message: "Make sure your device is connected to the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Gyms"
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gymDictionary.count
    }
    
    // Displaying gym names and addresses loacated in the zipcode entered by the user
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("gym_cell", forIndexPath: indexPath) 
        cell.textLabel?.text = gymNames[indexPath.row]
        cell.detailTextLabel?.text = gymDictionary[gymNames[indexPath.row]]
        cell.detailTextLabel?.textColor = UIColor.grayColor()
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    // MARK: - UITableViewDelegate
    
    // Called when user taps 'return' key on keypad
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Method that saves the gym selected by the user
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var fetchRequest = NSFetchRequest(entityName: "Gym")
        fetchRequest.predicate = NSPredicate(format: "gymName = %@", gymNames[gymTV.indexPathForSelectedRow!.row])
        let gym = try! managedObjectContext.executeFetchRequest(fetchRequest) as! [Gym]
        let gymOwner = gym[0].gymOwner
        (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym = gymOwner
        
        fetchRequest = NSFetchRequest(entityName: "PreviouslyEnteredGym")
        let gymToUpdate = try! managedObjectContext.executeFetchRequest(fetchRequest) as! [PreviouslyEnteredGym]
        
        if gymToUpdate.count == 0 {
            let previouslyEnteredGymEntityDescriptor = NSEntityDescription.entityForName("PreviouslyEnteredGym", inManagedObjectContext: self.managedObjectContext)
            let previouslyEnteredGym = NSManagedObject(entity: previouslyEnteredGymEntityDescriptor!, insertIntoManagedObjectContext: self.managedObjectContext) as! FitnessTrackerIOS.PreviouslyEnteredGym
            previouslyEnteredGym.gymOwner = gym[0].gymOwner
            previouslyEnteredGym.gymName = gym[0].gymName
            previouslyEnteredGym.gymID = gym[0].gymID
            previouslyEnteredGym.gymAddress = gym[0].gymAddress
            previouslyEnteredGym.gymCity = gym[0].gymCity
            previouslyEnteredGym.gymZipcode = gym[0].gymZipcode
            previouslyEnteredGym.gymState = gym[0].gymState
            
        } else {
            gymToUpdate[0].setValue(gym[0].gymOwner , forKey: "gymOwner")
            gymToUpdate[0].setValue(gym[0].gymName , forKey: "gymName")
            gymToUpdate[0].setValue(gym[0].gymID , forKey: "gymID")
            gymToUpdate[0].setValue(gym[0].gymAddress , forKey: "gymAddress")
            gymToUpdate[0].setValue(gym[0].gymCity , forKey: "gymCity")
            gymToUpdate[0].setValue(gym[0].gymZipcode , forKey: "gymZipcode")
            gymToUpdate[0].setValue(gym[0].gymState , forKey: "gymState")
        }
        
        
        try! managedObjectContext.save()
    }
    
    // Called when the user taps on the view (outside the TextField)
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

}
