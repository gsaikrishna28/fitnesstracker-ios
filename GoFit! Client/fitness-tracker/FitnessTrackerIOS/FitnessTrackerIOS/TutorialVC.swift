//
//  ParentContentViewController.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 7/5/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData

// This class represents the tutorial that appears when the user opens the app for the first time
class TutorialVC: UIViewController, UIPageViewControllerDataSource {
    
    var tutorialPageTitles : NSArray! // Contains titles for each page in tutorial
    var tutorialPageImages : NSArray! // Contains Images for each page in tutorial
    var pageViewController : UIPageViewController!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Tutorial screen titles
        self.tutorialPageTitles = NSArray(objects: "", "",  "", "", "", "", "", "", "")
        self.tutorialPageImages = NSArray(objects: "0", "1", "2","3", "4", "5", "6", "7", "8")
        
        
        // Instantiating a PageViewController
        
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("pageViewController") as! UIPageViewController
        
        self.pageViewController.dataSource = self
        
        let firstVC = self.viewControllerAtIndex(0) as TutorialTemplateVC
        let viewControllers = NSArray(object: firstVC)
        
        self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .Forward, animated: true, completion: nil )
        self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
    
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
        
        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        
        // Fetching info from CoreData to see if the app is being launched for the first time
        let fetch : NSFetchRequest = NSFetchRequest(entityName: "AppLaunch")
        let instructions = (try! managedObjectContext!.executeFetchRequest(fetch)) as! [AppLaunch]
        
        if instructions.count == 0 {
            let appLaunch =  NSEntityDescription.entityForName("AppLaunch", inManagedObjectContext: managedObjectContext!)
            let appLaunch1 = NSManagedObject(entity: appLaunch!, insertIntoManagedObjectContext:managedObjectContext) as! FitnessTrackerIOS.AppLaunch
            appLaunch1.isFirstLaunch = false
            //var error : NSError?
            do {
                try managedObjectContext!.save()
            } catch let error1 as NSError {
                print(error1)
            }
        }
    }
    
    // Implementing the PageViewController
    func viewControllerAtIndex(index : Int) -> TutorialTemplateVC {
        
        if ((self.tutorialPageTitles.count == 0) || (index >= self.tutorialPageTitles.count)) {
            return TutorialTemplateVC()
        }
        let contentVC : TutorialTemplateVC = self.storyboard?.instantiateViewControllerWithIdentifier("tutorialTemplateVC") as! TutorialTemplateVC
        
        contentVC.imageFile = self.tutorialPageImages[index] as! String
        contentVC.titleText = self.tutorialPageTitles[index] as! String
        contentVC.pageIndex = index
        
        return contentVC
    }
    
    // Mark : - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let viewcontroller = viewController as! TutorialTemplateVC
        var index = viewcontroller.pageIndex
        if ((index == 0) || (index == NSNotFound)) {
            return nil
        }
        index?--
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let viewcontroller = viewController as! TutorialTemplateVC
        var index = viewcontroller.pageIndex
        if (index == NSNotFound) {
            return nil
        }
        index?++
        if index == self.tutorialPageTitles.count {
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.tutorialPageTitles.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
