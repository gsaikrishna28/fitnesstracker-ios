//
//  ViewController.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 6/30/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import UIKit
import CoreData
import Parse
import SystemConfiguration

// This class represents the Workout-Design tab
class HomeScreenVC: UIViewController, UITableViewDataSource, UITableViewDelegate, ESTBeaconManagerDelegate{
    
    // The TableView that displays the machines selected by the user
    @IBOutlet weak var userWorkoutsTV: UITableView!
    
    var managedObjectContext : NSManagedObjectContext!
    var selectedGymMachines : [Machine] = []
    var adminForSelectedGym : String = ""
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    
    // Instantiating a property that holds the beacon manager
    let beaconManager = ESTBeaconManager()
    
    // Defining a beacon region using the UUID of the beacon
    let beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "Estimotes")
    
    @IBAction func changegymBTN(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.presentViewController(storyboard.instantiateViewControllerWithIdentifier("searchingGymsNVC") as! UINavigationController, animated: true, completion: nil)
        
    }
    
    
    // Method that displays the tutorial again
    @IBAction func viewTutorial(sender: AnyObject) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.presentViewController(storyboard.instantiateViewControllerWithIdentifier("tutorialVC") , animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        adminForSelectedGym = (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym
        
        fetchDataFromParse()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 36/255, green: 73/255, blue: 134/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.beaconManager.delegate = self
        
        // Requesting location access "Always"
        self.beaconManager.requestAlwaysAuthorization()
        self.beaconManager.avoidUnknownStateBeacons = true
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if Reachability.isConnectedToNetwork() == false {
            let alert = UIAlertController(title:"No Internet Connection :(", message: "Make sure your device is connected to the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
        
        adminForSelectedGym = (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym
        
        var fetch = NSFetchRequest(entityName: "Gym")
        fetch.predicate = NSPredicate(format: "gymOwner = %@", adminForSelectedGym)
        self.navigationItem.title = (try! self.managedObjectContext.executeFetchRequest(fetch) as! [Gym])[0].gymName
        
        self.deleteDataNotThereInParse()
        
        // Fetching mahicnes information
        var machineCount : Int32 = 0
        fetch = NSFetchRequest(entityName: "Machine")
        for machine in (try! self.managedObjectContext.executeFetchRequest(fetch)) as! [Machine] {
            if machine.belongsTo.gymOwner == adminForSelectedGym {
                machineCount++
            }
        }
        
        let query = PFQuery(className: "GymEquipments")
        query.whereKey("username", equalTo:self.adminForSelectedGym)
        query.countObjectsInBackgroundWithBlock {
            (count: Int32, error: NSError?) -> Void in
            if error == nil {
                if machineCount != count {
                    
                    self.updateDataFromParse()
                }
            }
        }
        
        selectedGymMachines = [Machine]()
        
        fetch.predicate = NSPredicate(format: "machineSelected==%@", true)
        fetch.sortDescriptors = [NSSortDescriptor(key: "machineName", ascending: true)]
        for machine in (try! self.managedObjectContext.executeFetchRequest(fetch)) as! [Machine] {
            if machine.belongsTo.gymOwner == adminForSelectedGym {
                selectedGymMachines.append(machine)
            }
        }
        userWorkoutsTV.reloadData()
        
        
        // Stops monitoring and ranging for the beacons
        self.beaconManager.stopMonitoringForRegion(self.beaconRegion)
        self.beaconManager.stopRangingBeaconsInRegion(self.beaconRegion)
        
    }
    
    // Fetching entire information about the gym equipments from parse
    func fetchDataFromParse() {
        let fetchRequest = NSFetchRequest(entityName: "MachineDetailsAddedFromParse")
        fetchRequest.predicate = NSPredicate(format: "gymName = %@", (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym)
        if ((try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [MachineDetailsAddedFromParse]).count == 0 {
            
            // Instantiating and starting activity indicator (like Loading...)
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            let fetchRequest = NSFetchRequest(entityName: "Gym")
            fetchRequest.predicate = NSPredicate(format: "gymOwner = %@", self.adminForSelectedGym)
            let gym = ((try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [Gym])[0]
            
            // Fetching data from Parse - GymEquipments table
            let query = PFQuery(className: "GymEquipments")
            // Condition to fetch machines for the selected gym
            query.whereKey("username", equalTo:self.adminForSelectedGym)
            query.findObjectsInBackgroundWithBlock {
                (gymEquipments: [PFObject]?, error: NSError?) -> Void in
                activityIndicator.startAnimating()
                if error == nil {
                    if let gymEquipments = gymEquipments {
                        for gymEquipment in gymEquipments {
                            let machineEntityDescriptor =  NSEntityDescription.entityForName("Machine", inManagedObjectContext: self.managedObjectContext)
                            let machine = NSManagedObject(entity: machineEntityDescriptor!, insertIntoManagedObjectContext:self.managedObjectContext) as! FitnessTrackerIOS.Machine
                            machine.machineName = gymEquipment.valueForKey("equipmentName") as! String
                            machine.machineID = gymEquipment.objectId!
                            machine.machineSelected = false
                            machine.belongsTo = gym
                            var machines = gym.hasMacines.allObjects as! [Machine]
                            machines.append(machine)
                            gym.setValue(NSSet(array:machines) , forKey: "hasMacines")
                            var err : NSError?
                            do {
                                try self.managedObjectContext.save()
                            } catch let error as NSError {
                                err = error
                                print(err)
                            } catch {
                                fatalError()
                            }
                            activityIndicator.stopAnimating()
                        }
                    }
                }
            }
            activityIndicator.stopAnimating()
            
            let exercisesAddedFromParseEntityDescriptor = NSEntityDescription.entityForName("MachineDetailsAddedFromParse", inManagedObjectContext: self.managedObjectContext)
            let exercisesAddedFromParse = NSManagedObject(entity: exercisesAddedFromParseEntityDescriptor!, insertIntoManagedObjectContext: self.managedObjectContext) as! FitnessTrackerIOS.MachineDetailsAddedFromParse
            exercisesAddedFromParse.gymName = (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym
            exercisesAddedFromParse.machinesAdded = true
            do {
                try self.managedObjectContext.save()
            } catch _ {
            }
        }
    }
    
    
    // Invoked when the admin makes any changes to his gym(adding machines)
    func updateDataFromParse() {
        
        let fetchRequest = NSFetchRequest(entityName: "Gym")
        fetchRequest.predicate = NSPredicate(format: "gymOwner = %@", self.adminForSelectedGym)
        let gym = ((try! self.managedObjectContext.executeFetchRequest(fetchRequest)) as! [Gym])[0]
        
        let query = PFQuery(className: "GymEquipments")
        query.whereKey("username", equalTo:self.adminForSelectedGym)
        query.findObjectsInBackgroundWithBlock {
            (gymEquipments: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let gymEquipments = gymEquipments {
                    for gymEquipment in gymEquipments {
                        var machineCount = 0
                        let fetchRequest = NSFetchRequest(entityName: "Machine")
                        fetchRequest.predicate = NSPredicate(format: "machineName = %@", gymEquipment.valueForKey("equipmentName") as! String)
                        let machines = (try! self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Machine])
                        for machine in machines {
                            if machine.belongsTo.gymOwner == self.adminForSelectedGym {
                                machineCount++
                            }
                        }
                        
                        if machineCount == 0 {
                            let machineEntityDescriptor =  NSEntityDescription.entityForName("Machine", inManagedObjectContext: self.managedObjectContext)
                            let machine = NSManagedObject(entity: machineEntityDescriptor!, insertIntoManagedObjectContext:self.managedObjectContext) as! FitnessTrackerIOS.Machine
                            machine.machineName = gymEquipment.valueForKey("equipmentName") as! String
                            machine.machineID = gymEquipment.objectId!
                            machine.machineSelected = false
                            machine.belongsTo = gym
                            var machines = gym.hasMacines.allObjects as! [Machine]
                            machines.append(machine)
                            gym.setValue(NSSet(array:machines) , forKey: "hasMacines")
                            self.userWorkoutsTV.reloadData()
                            var err : NSError?
                            do {
                                try self.managedObjectContext.save()
                            } catch let error as NSError {
                                err = error
                                print(err)
                            } catch {
                                fatalError()
                            }
                            
                        }
                        
                        
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    
    // Invoked when the admin makes any changes to his gym(deleting machines)
    func deleteDataNotThereInParse() {
        
        let fetchRequest = NSFetchRequest(entityName: "Machine")
        for machine in (try! self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Machine]) {
            if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                let query = PFQuery(className: "GymEquipments")
                query.whereKey("equipmentName", equalTo:machine.machineName)
                query.whereKey("username", equalTo:self.adminForSelectedGym)
                query.countObjectsInBackgroundWithBlock {
                    (count: Int32, error: NSError?) -> Void in
                    if error == nil {
                        if count == 0 {
                            self.managedObjectContext.deleteObject(machine)
                            self.selectedGymMachines.removeAtIndex(self.selectedGymMachines.indexOf(machine)!)
                            self.userWorkoutsTV.reloadData()
                            do {
                                try self.managedObjectContext.save()
                            } catch {
                                fatalError()
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    // MARK: - UITableViewDataSource
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedGymMachines.count
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Machines"
    }
    
    
    // Displaying the machine names in the cells
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("exercise_cell", forIndexPath: indexPath)
        cell.textLabel?.text = selectedGymMachines[indexPath.row].machineName
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            selectedGymMachines.removeAtIndex(indexPath.row)
            
            var requiredMachines = [Machine]()
            
            let fetch : NSFetchRequest = NSFetchRequest(entityName: "Machine")
            fetch.predicate = NSPredicate(format: "machineSelected==%@", true)
            fetch.sortDescriptors = [NSSortDescriptor(key: "machineName", ascending: true)]
            
            for machine in (try! managedObjectContext.executeFetchRequest(fetch)) as! [Machine] {
                if machine.belongsTo.gymOwner == (UIApplication.sharedApplication().delegate as! AppDelegate).adminForSelectedGym {
                    requiredMachines.append(machine)
                }
            }
            
            requiredMachines[indexPath.row].setValue(false, forKey: "machineSelected")
            
            do {
                try managedObjectContext.save()
            } catch _ {
            }
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        (UIApplication.sharedApplication().delegate as! AppDelegate).machineRow = userWorkoutsTV.indexPathForSelectedRow!.row
        (UIApplication.sharedApplication().delegate as! AppDelegate).selectedMachine = selectedGymMachines[userWorkoutsTV.indexPathForSelectedRow!.row]
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
}
