//
//  Machine.swift
//  FitnessTrackerIOS
//
//  Created by Admin on 9/23/15.
//  Copyright (c) 2015 edu. All rights reserved.
//

import Foundation
import CoreData

// This class represents the information that is stored by each machine
class Machine: NSManagedObject {

    @NSManaged var machineID: String
    @NSManaged var machineName: String
    @NSManaged var machineSelected: NSNumber
    @NSManaged var belongsTo: Gym
    @NSManaged var hasBeacon: Beacon
    @NSManaged var hasExercise: NSSet

}
