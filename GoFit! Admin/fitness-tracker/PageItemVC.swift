//
//  PageItemVC.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 11/11/15.
//  Copyright © 2015 Techbees. All rights reserved.
//

import UIKit

class PageItemVC: UIViewController {
    
    // MARK: - Variables
    var itemIndex: Int = 0
    
   
    
    var imageName: String = "" {
        
        didSet {
            if let imageView = contentImageView {
                imageView.image = UIImage(named: imageName)
            }
        }
    }
    
    var tutorialScreenTitle: String = "" {
        
        didSet {
            if let titleLBL = titleLBL {
                titleLBL.text = tutorialScreenTitle
            }
        }
    }
    
    @IBOutlet weak var titleLBL: UILabel!

    @IBOutlet weak var contentImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentImageView!.image = UIImage(named: imageName)
        self.titleLBL.text = tutorialScreenTitle
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
