//
//  CreateAccountVC.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 8/9/15.
//  Copyright (c) 2015 Techbees. All rights reserved.
//

import UIKit
import Parse

// This class represents the Create Account screen
class CreateAccountVC: UIViewController, UITextFieldDelegate {
    
    var appy:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var usernameMsgLBL: UILabel!
    @IBOutlet weak var emailMsgLBL: UILabel!
    
    @IBAction func usernameTFACN(sender: AnyObject) {
        if self.usernameMsgLBL.text == "Username already exists" {
            self.usernameMsgLBL.text = ""
        }
        else if self.usernameTF.text?.characters.count >= 6 {
            self.usernameMsgLBL.text = ""
        }
        
    }
    @IBAction func emailTFACN(sender: AnyObject) {
        if self.emailMsgLBL.text == "Email ID already exists" {
            self.emailMsgLBL.text = ""
        }
    }
    
    @IBAction func confirmPasswordTFACN(sender: AnyObject) {
        if self.alertMessageLBL.text == "Password mismatch" {
            self.alertMessageLBL.text = ""
        }
    }
    
    
    @IBAction func emailAddressTFACN(sender: AnyObject) {
        self.emailMsgLBL.text = ""
    }
    
    @IBOutlet weak var alertMessageLBL: UILabel!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    // Method invoked when the user click on "Create Account"
    @IBAction func create(sender: AnyObject) {
        
        // Checking if any textfields were left empty
        if self.usernameTF.text == "" || self.passwordTF.text == "" || self.confirmPasswordTF.text == "" || self.emailTF.text == "" {
            let alert = UIAlertController(title: "", message:"Please enter all the fields", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
        else {
            let query = PFQuery(className: "_User")
            query.whereKey("email", equalTo: self.emailTF.text!)
            query.countObjectsInBackgroundWithBlock { (count: Int32, error:NSError?) -> Void in
                if error == nil {
                    if count > 0 {
                        self.emailMsgLBL.text = "Email ID already exists"
                        self.emailTF.text = ""
                    } else if !self.validateEmail(self.emailTF.text!) {
                        self.emailMsgLBL.text = "Invalid email address"
                    }
                    else {
                        // If both the entered passwords match
                        if(self.passwordTF.text == self.confirmPasswordTF.text) {
                            self.appy.message = "Account created successfully :)"
                            let user = PFUser()
                            user.username = self.usernameTF.text
                            user.password = self.passwordTF.text
                            user.email = self.emailTF.text
                            user.signUpInBackgroundWithBlock({ (success : Bool, error : NSError?) -> Void in
                                if error == nil {
                                    
                                } else {
                                    
                                }
                            })
                            self.dismissViewControllerAnimated(true, completion: nil)
                            PFUser.logOutInBackground()
                        }
                        else{
                            self.alertMessageLBL.text = "Password mismatch"
                            self.confirmPasswordTF.text = ""
                        }
                    }
                    
                }
                
            }
            
            
            
        }
        
    }
    
    // Method invoked if the user presses "Cancel" button
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255, green: 82/255, blue: 139/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        
        self.usernameMsgLBL.text = ""
        self.emailMsgLBL.text = ""
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.alertMessageLBL.text = ""
        self.usernameTF.text = ""
        self.passwordTF.text = ""
        self.confirmPasswordTF.text = ""
        super.viewWillAppear(true)
    }
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string.characters.count == 0 {
            return true
        }
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        if textField == self.usernameTF {
            return prospectiveText.doesNotContainCharactersIn(" ")
        }
        return true
        
    }
    
    // Validating username
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == self.usernameTF {
            let query = PFQuery(className: "_User")
            query.whereKey("username", equalTo: self.usernameTF.text!)
            query.countObjectsInBackgroundWithBlock { (count: Int32, error:NSError?) -> Void in
                if error == nil {
                    if self.usernameTF.text?.characters.count < 6 {
                        self.usernameMsgLBL.text = "Username must have at least 6 characters"
                        self.usernameTF.becomeFirstResponder()
                    }
                    else if count > 0 {
                        self.usernameMsgLBL.text = "Username already exists"
                        self.usernameTF.text = ""
                        self.usernameTF.becomeFirstResponder()
                    }
                }
                
            }
        }
            
        else if textField == self.confirmPasswordTF {
            if self.passwordTF.text != self.confirmPasswordTF.text {
                self.alertMessageLBL.text = "Password mismatch"
                self.confirmPasswordTF.text = ""
            }
        }
        else if textField == self.emailTF {
            let query = PFQuery(className: "_User")
            query.whereKey("email", equalTo: self.emailTF.text!)
            query.countObjectsInBackgroundWithBlock { (count: Int32, error:NSError?) -> Void in
                if error == nil {
                    if count > 0 {
                        self.emailMsgLBL.text = "Email ID already exists"
                        self.emailTF.text = ""
                    }
                    
                }
                
            }
        }
    
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}
