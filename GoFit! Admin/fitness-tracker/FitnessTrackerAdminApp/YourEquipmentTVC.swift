//
//  YourEquipmentTVC.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 7/27/15.
//  Copyright (c) 2015 Techbees. All rights reserved.
//

import UIKit
import Parse

// This class represents the machines selected by the gym owner
class YourEquipmentTVC: UITableViewController {
    
    var appy:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var yourEquipments = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255, green: 82/255, blue: 139/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    
    // Fetching the machines list form parse
    override func viewWillAppear(animated: Bool) {
        
        let query = PFQuery(className: "GymEquipments")
        query.findObjectsInBackgroundWithBlock { (gymEquipments:[AnyObject]?, error: NSError?) -> Void in
            if error == nil{
                if let equipments = gymEquipments as? [PFObject]{
                    self.yourEquipments = [String]()
                    for equipment in equipments {
                        if self.appy.username == equipment["username"] as! String {
                            self.yourEquipments.append(equipment["equipmentName"] as! String)
                        }
                    }
                    self.yourEquipments.sortInPlace{$0<$1}
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tableView.reloadData()
                    }
                    
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.yourEquipments.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Configure the cell...
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell1")
        cell!.textLabel?.text = self.yourEquipments[indexPath.row]
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        //var row = self.tableView.indexPathForSelectedRow()!.row
        self.appy.equipmentName = cell.textLabel!.text!
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Your Equipment"
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    
    // Implementing editing functionality for machines and beacons
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)!
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let cellText:String = (cell.textLabel?.text)!
            
            let equipmentQuery = PFQuery(className: "GymEquipments")
            equipmentQuery.findObjectsInBackgroundWithBlock { (gymEquipments:[AnyObject]?, error: NSError?) -> Void in
                if error == nil{
                    if let equipments = gymEquipments as? [PFObject]{
                        
                        for equipment in equipments {
                            if cellText == equipment["equipmentName"] as? String {
                                equipment.deleteInBackground()
                                self.yourEquipments.removeAtIndex(indexPath.row)
                                self.tableView.reloadData()
                                break
                            }
                        }
                    }
                }
            }
            // If a machine is deleted, the beacon associated with that machine is also deleted
            let beaconQuery = PFQuery(className: "BeaconDetails")
            beaconQuery.findObjectsInBackgroundWithBlock { (beacons:[AnyObject]?, error: NSError?) -> Void in
                if error == nil{
                    if let beacons = beacons as? [PFObject]{
                        for beacon in beacons {
                            if cellText == beacon["equipmentName"] as? String && self.appy.username == beacon["username"] as? String {
                                beacon.deleteInBackground()
                                break
                            }
                        }
                    }
                }
            }
        }
    }
}
