//
//  ForgetPasswordViewController.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 10/22/15.
//  Copyright © 2015 Techbees. All rights reserved.
//

import UIKit
import Parse

// This class represnts the "Forgot Password" screen
class ForgetPasswordViewController: UIViewController {
    
    // Method to validate user entered email
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
    }
    
    @IBOutlet weak var emailAddressTF: UITextField!
    
    // Method invoked when the user presses "Reset Password" button
    @IBAction func resetPasswordBTN(sender: AnyObject) {
        
        let emailAddress = emailAddressTF.text
        
        if emailAddress!.isEmpty {
            let alert = UIAlertController(title: "", message:"Please enter your email address", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
        else {
            if !(validateEmail(emailAddress!)) {
                let alert = UIAlertController(title: "", message:"Enter a valid email address", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            }
            // Requesting password reset from parse
            // An email will be sent to the admin from parse for password reset
            else {
                PFUser.requestPasswordResetForEmailInBackground(emailAddress!) { (success : Bool, error : NSError?) -> Void in
                    if error == nil {
                        let alert = UIAlertController(title: "", message:"An email message was sent to \(emailAddress!)", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                        self.presentViewController(alert, animated: true){}
                    } else {
                        var errorMessage : String =  (error?.localizedDescription)!
                        errorMessage = errorMessage.stringByReplacingCharactersInRange(errorMessage.startIndex...errorMessage.startIndex, withString: String(errorMessage[errorMessage.startIndex]).uppercaseString)
                        let alert = UIAlertController(title: "", message:"\(errorMessage)", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                        self.presentViewController(alert, animated: true){}
                    }
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
