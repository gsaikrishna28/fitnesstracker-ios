//
//  GymInfoVC.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 7/27/15.
//  Copyright (c) 2015 Techbees. All rights reserved.
//

import UIKit
import Parse

// This class represents a gym and shows the gym information
class GymInfoVC: UIViewController, UITextFieldDelegate, UIPickerViewDelegate {
    
    var appy:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var isItNew:Bool = true
    
    // Regular exoression to allow only valid US/Canada zipcodes
    let zipcodeRegEx: NSRegularExpression = try! NSRegularExpression(pattern: "(^[A-Za-z]\\d[A-Za-z][ ]?\\d[A-Za-z]\\d$)|(^\\d{5}(?:[-\\s]\\d{4})?$)", options: NSRegularExpressionOptions.CaseInsensitive)
    
    // Checking if the zipcode entered is valid
    func testZipcode(input: String) -> Bool {
        let matches = self.zipcodeRegEx.matchesInString(input, options: NSMatchingOptions.ReportCompletion, range:NSMakeRange(0, input.characters.count))
        return matches.count > 0
    }
    
    // Declaring all the 50 states
    var states = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]
    
    @IBOutlet weak var gymNameTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var zipcodeTF: UITextField!
    
    @IBOutlet weak var statePV: UIPickerView! // Picker view for selection of states
    
    @IBOutlet weak var zipcodeEXLBL: UILabel!
    @IBOutlet weak var saveBTN: UIButton!
    @IBOutlet weak var zipcodeMsgLBL: UILabel!
    
    
    // Method invoked when the user presses logout button
    // After logging out, the user see the login screen again
    @IBAction func logoutBTN(sender: AnyObject) {
        PFUser.logOut()
        self.dismissViewControllerAnimated(true, completion: nil)
        let loginNavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("loginNC") as! UINavigationController
        self.presentViewController(loginNavigationController, animated: true, completion: nil)
    }
    
    // Method invoked when the user presses Edit/Save button to edit gym information
    @IBAction func saveOrEditBTN(sender: AnyObject) {
        
        // If the gym information is being edited
        if saveBTN.titleLabel?.text == "Save" {
            
            // Checking if any textfields are left empty
            if self.gymNameTF.text == "" || self.addressTF.text == "" || self.cityTF.text == "" || self.stateTF.text == "" || self.zipcodeTF.text == "" {
                let alert = UIAlertController(title: "", message:"Please enter all the fields", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            }
            
            // Validating zipcode
            else if !self.testZipcode(self.zipcodeTF.text!)  {
                self.zipcodeMsgLBL.text = "Invalid zipcode!"
                self.zipcodeTF.becomeFirstResponder()
            }
            else {
                self.zipcodeMsgLBL.text = ""
                self.saveBTN.setTitle("Edit", forState: UIControlState.Normal)
                self.statePV.hidden = true
                self.zipcodeEXLBL.hidden = true
                
                self.gymNameTF.enabled = false
                self.addressTF.enabled = false
                self.cityTF.enabled = false
                self.stateTF.enabled = false
                self.zipcodeTF.enabled = false
                self.gymNameTF.borderStyle = UITextBorderStyle.None
                self.addressTF.borderStyle = UITextBorderStyle.None
                self.cityTF.borderStyle = UITextBorderStyle.None
                self.stateTF.borderStyle = UITextBorderStyle.None
                self.zipcodeTF.borderStyle = UITextBorderStyle.None
                self.gymNameTF.textColor = UIColor.grayColor()
                self.addressTF.textColor = UIColor.grayColor()
                self.cityTF.textColor = UIColor.grayColor()
                self.stateTF.textColor = UIColor.grayColor()
                self.zipcodeTF.textColor = UIColor.grayColor()
                
                // If a new gym is being set up
                if isItNew == true {
                    let gymInfo = PFObject(className: "GymInfo")
                    gymInfo["gymName"] = self.gymNameTF.text
                    gymInfo["address"] = self.addressTF.text
                    gymInfo["city"] = self.cityTF.text
                    gymInfo["state"] = self.stateTF.text
                    gymInfo["zipcode"] = self.zipcodeTF.text
                    gymInfo["username"] = self.appy.username
                    gymInfo.saveInBackground()
                }
                    
                // If the gym already exists, and the information is being changed
                else {
                    let query = PFQuery(className: "GymInfo")
                    query.findObjectsInBackgroundWithBlock { (gyms:[AnyObject]?, error: NSError?) -> Void in
                        if error == nil{
                            if let objects = gyms as? [PFObject] {
                                for object in objects {
                                    if(self.appy.username == object["username"] as! String) {
                                        object["gymName"] = self.gymNameTF.text
                                        object["address"] = self.addressTF.text
                                        object["city"] = self.cityTF.text
                                        object["state"] = self.stateTF.text
                                        object["zipcode"] = self.zipcodeTF.text
                                        object.saveInBackground()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
            
        // If the gym information is already saved
        else if saveBTN.titleLabel?.text == "Edit" {
            isItNew = false
            self.saveBTN.setTitle("Save", forState: UIControlState.Normal)
            
            self.zipcodeEXLBL.hidden = false
            
            self.gymNameTF.enabled = true
            self.addressTF.enabled = true
            self.cityTF.enabled = true
            self.stateTF.enabled = true
            self.zipcodeTF.enabled = true
            self.gymNameTF.borderStyle = UITextBorderStyle.RoundedRect
            self.addressTF.borderStyle = UITextBorderStyle.RoundedRect
            self.cityTF.borderStyle = UITextBorderStyle.RoundedRect
            self.stateTF.borderStyle = UITextBorderStyle.RoundedRect
            self.zipcodeTF.borderStyle = UITextBorderStyle.RoundedRect
            self.gymNameTF.textColor = UIColor.blackColor()
            self.addressTF.textColor = UIColor.blackColor()
            self.cityTF.textColor = UIColor.blackColor()
            self.stateTF.textColor = UIColor.blackColor()
            self.zipcodeTF.textColor = UIColor.blackColor()
            
        }
    }
    
    
    override func viewDidLoad() {
          super.viewDidLoad()
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: "", message:"Make sure your device is connected to the internet.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
        
        states = states.sort{$0<$1}
        
        // Setting the navigation bar properties
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255, green: 82/255, blue: 139/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.zipcodeMsgLBL.text = ""
        
        
        // Fetching gym information from parse
        let query = PFQuery(className: "GymInfo")
        query.findObjectsInBackgroundWithBlock { (credentials:[AnyObject]?, error: NSError?) -> Void in
            if error == nil{
                if let objects = credentials as? [PFObject] {
                    for object in objects {
                        if(self.appy.username == object["username"] as! String) {
                            
                            self.zipcodeEXLBL.hidden = true
                            
                            self.gymNameTF.enabled = false
                            self.addressTF.enabled = false
                            self.cityTF.enabled = false
                            self.stateTF.enabled = false
                            self.zipcodeTF.enabled = false
                            self.gymNameTF.borderStyle = UITextBorderStyle.None
                            self.addressTF.borderStyle = UITextBorderStyle.None
                            self.cityTF.borderStyle = UITextBorderStyle.None
                            self.stateTF.borderStyle = UITextBorderStyle.None
                            self.zipcodeTF.borderStyle = UITextBorderStyle.None
                            self.gymNameTF.textColor = UIColor.grayColor()
                            self.addressTF.textColor = UIColor.grayColor()
                            self.cityTF.textColor = UIColor.grayColor()
                            self.stateTF.textColor = UIColor.grayColor()
                            self.zipcodeTF.textColor = UIColor.grayColor()
                            
                            
                            self.gymNameTF.text = object["gymName"] as? String
                            self.addressTF.text = object["address"] as? String
                            self.cityTF.text = object["city"] as? String
                            self.stateTF.text = object["state"] as? String
                            self.zipcodeTF.text = object["zipcode"] as? String
                            
                            var row = 0;
                            for var i=0; i<self.states.count; i++ {
                                if self.states[i] == self.stateTF.text {
                                    row = i;
                                }
                            }
                            self.statePV.selectRow(row, inComponent: 0, animated: true)
                            self.saveBTN.setTitle("Edit", forState: UIControlState.Normal)
                        }
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.states.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.states[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.stateTF.text = self.states[row]
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == self.stateTF {
            self.statePV.hidden = false
        }
        else {
            self.statePV.hidden = true
        }
    }
    
    
    // Validating the textfields
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string.characters.count == 0 {
            return true
        }
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        
        switch textField {
            
        case cityTF:
            return prospectiveText.containsOnlyCharactersIn("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ")
            
        case stateTF:
            return prospectiveText.containsOnlyCharactersIn("ABCDEFGHIJKLMNOPQRSTUVWXYZ") &&
                prospectiveText.characters.count <= 2
            
        case zipcodeTF:
            return prospectiveText.containsOnlyCharactersIn("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789- ") &&
                prospectiveText.characters.count <= 10
            
            
        default: return true
            
        }
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        self.statePV.hidden = true
    }
    
}
