
//  AssigningBeaconVC.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 9/19/15.
//  Copyright (c) 2015 Techbees. All rights reserved.
//

import UIKit
import Parse


// This class represents assigning a beacon to each machine
class AssigningBeaconVC: UIViewController, UITextFieldDelegate, ESTBeaconManagerDelegate {
    
    var appy:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var isItNew:Bool = true
    
    // Default UUID for estimote beacons
    let defaultUUID:String = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"
    
    let beaconManager = ESTBeaconManager()
    // Declaring a beacon region using the estimote default UUID
    let beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "ranged region")
    
    var closestBeacon:CLBeacon!
    var beacons:[CLBeacon] = []
    
    @IBOutlet weak var equipmentNameLBL: UILabel!
    
    @IBOutlet weak var scanningLBL: UILabel!
    @IBOutlet weak var scanningSwitch: UISwitch!
    
    // This method starts/stops scanning for the beacons when the user toggles the "Scan" switch
    @IBAction func scan(sender: UISwitch) {
        if self.scanningSwitch.on {
            self.beaconManager.startRangingBeaconsInRegion(self.beaconRegion)
            self.noteLBL.hidden = false
        }
        else {
            self.beaconManager.stopRangingBeaconsInRegion(self.beaconRegion)
            self.noteLBL.hidden = true
        }
        self.uuid1TF.resignFirstResponder()
        self.uuid2TF.resignFirstResponder()
        self.uuid3TF.resignFirstResponder()
        self.uuid4TF.resignFirstResponder()
        self.uuid5TF.resignFirstResponder()
        self.majorIDTF.resignFirstResponder()
        self.minorIDTF.resignFirstResponder()
    }
    
    @IBOutlet weak var majorIDTF: UITextField!
    @IBOutlet weak var minorIDTF: UITextField!
    
    @IBOutlet weak var noteLBL: UILabel!
    @IBOutlet weak var uuid1TF: UITextField!
    @IBOutlet weak var uuid2TF: UITextField!
    @IBOutlet weak var uuid3TF: UITextField!
    @IBOutlet weak var uuid4TF: UITextField!
    @IBOutlet weak var uuid5TF: UITextField!
    @IBOutlet weak var uuidEXLBL: UILabel!
    @IBOutlet weak var majorIDEXLBL: UILabel!
    @IBOutlet weak var minorIDEXLBL: UILabel!
    
    
     // These methods are used to validate the beacon ID's
    // The beacon UUID follows a 8-4-4-4-12 pattern
    // The major ID can be any value from 0-65535
    // The minor ID can be any value from 0-65535
    @IBAction func uuid1TFV(sender: AnyObject) {
        if uuid1TF.text?.characters.count == 8 {
            self.uuid2TF.enabled = true
            self.uuid2TF.becomeFirstResponder()
        }
    }
   
    @IBAction func uuid2TFV(sender: AnyObject) {
        if uuid2TF.text?.characters.count == 4 {
            self.uuid3TF.enabled = true
            self.uuid3TF.becomeFirstResponder()
        }
    }
    
    @IBAction func uuid3TFV(sender: AnyObject) {
        if uuid3TF.text?.characters.count == 4 {
            self.uuid4TF.enabled = true
            self.uuid4TF.becomeFirstResponder()
        }
    }
    
    
    @IBAction func uuid4TFV(sender: AnyObject) {
        if uuid4TF.text?.characters.count == 4 {
            self.uuid5TF.enabled = true
            self.uuid5TF.becomeFirstResponder()
        }
    }
    
    @IBAction func majorIDTFV(sender: AnyObject) {
        if NSString(string: self.majorIDTF.text!).integerValue > 65535 {
            self.majorIDTF.text = ""
            self.majorIDTF.becomeFirstResponder()
        }
    }
    
    @IBAction func minorIDTFV(sender: AnyObject) {
        if NSString(string: self.minorIDTF.text!).integerValue > 65535 {
            self.minorIDTF.text = ""
            self.minorIDTF.becomeFirstResponder()
        }
    }
    // End of methods for validation of beacon ID's
    
    
    
    @IBOutlet weak var saveBTN: UIButton!
    
    // Method invoked when the user presses Edit/Save button
    @IBAction func saveOrEditBTN(sender: AnyObject) {
        
        // If the beacon details are currently being edited
        if saveBTN.titleLabel?.text == "Save" {
            
            
            // If any textfields are left empty
            if self.uuid1TF.text == "" || self.uuid2TF.text == "" || self.uuid3TF.text == "" || self.uuid4TF.text == "" || self.uuid5TF.text == "" || self.majorIDTF.text == "" || self.minorIDTF.text == "" {
                let alert = UIAlertController(title: "", message:"Please enter all the fields", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            }
            
            // If the beacon ID's are not valid
            else if uuid1TF.text?.characters.count < 8 || uuid2TF.text?.characters.count < 4 || uuid3TF.text?.characters.count < 4 || uuid4TF.text?.characters.count < 4 || uuid5TF.text?.characters.count < 12 {
                let alert = UIAlertController(title: "", message:"Please enter a valid UUID", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            }
                
            // If the beacon ID's follow the universal requirements
            else {
                
                if self.scanningSwitch.on {
                    self.beaconManager.startRangingBeaconsInRegion(self.beaconRegion)
                }
                self.saveBTN.setTitle("Edit", forState: UIControlState.Normal)
                self.beaconManager.stopRangingBeaconsInRegion(self.beaconRegion)
                self.noteLBL.hidden = true
                self.scanningLBL.hidden = true
                self.scanningSwitch.hidden = true
                
                self.uuid1TF.enabled = false
                self.uuid2TF.enabled = false
                self.uuid3TF.enabled = false
                self.uuid4TF.enabled = false
                self.uuid5TF.enabled = false
                self.majorIDTF.enabled = false
                self.majorIDTF.borderStyle = UITextBorderStyle.None
                self.minorIDTF.enabled = false
                self.minorIDTF.borderStyle = UITextBorderStyle.None
                
                self.uuidEXLBL.hidden = true
                self.majorIDEXLBL.hidden = true
                self.minorIDEXLBL.hidden = true
                
                // If a new beacon is beacon is being assigned to a machine
                let uuid : String = String(format: "%@-%@-%@-%@-%@", self.uuid1TF.text!, self.uuid2TF.text!, self.uuid3TF.text!, self.uuid4TF.text!, self.uuid5TF.text!)
                if isItNew == true {
                    let beaconInfo = PFObject(className: "BeaconDetails")
                    beaconInfo["uuid"] = uuid
                    beaconInfo["majorID"] = self.majorIDTF.text
                    beaconInfo["minorID"] = self.minorIDTF.text
                    beaconInfo["equipmentName"] = self.appy.equipmentName
                    beaconInfo["username"] = self.appy.username
                    beaconInfo.saveInBackground()
                }
                
                // If a beacon already exists, and it is being edited for a machine
                else {
                    let query = PFQuery(className: "BeaconDetails")
                    query.findObjectsInBackgroundWithBlock { (beacons:[AnyObject]?, error: NSError?) -> Void in
                        if error == nil{
                            if let objects = beacons as? [PFObject] {
                                for object in objects {
                                    if(self.appy.username == object["username"] as! String && self.appy.equipmentName == object["equipmentName"] as! String) {
                                        object["uuid"] = uuid
                                        object["majorID"] = self.majorIDTF.text
                                        object["minorID"] = self.minorIDTF.text
                                        object.saveInBackground()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
            
        // If the beacon information is already saved
        else if saveBTN.titleLabel?.text == "Edit" {
            
            isItNew = false
            self.saveBTN.setTitle("Save", forState: UIControlState.Normal)
            
            self.noteLBL.hidden = false
            self.scanningLBL.hidden = false
            self.scanningSwitch.hidden = false
            self.scan(self.scanningSwitch)
            
            self.uuid1TF.enabled = true
            self.uuid1TF.borderStyle = UITextBorderStyle.RoundedRect
            self.uuid2TF.enabled = true
            self.uuid2TF.borderStyle = UITextBorderStyle.RoundedRect
            self.uuid3TF.enabled = true
            self.uuid3TF.borderStyle = UITextBorderStyle.RoundedRect
            self.uuid4TF.enabled = true
            self.uuid4TF.borderStyle = UITextBorderStyle.RoundedRect
            self.uuid5TF.enabled = true
            self.uuid5TF.borderStyle = UITextBorderStyle.RoundedRect
            self.majorIDTF.enabled = true
            self.majorIDTF.borderStyle = UITextBorderStyle.RoundedRect
            self.minorIDTF.enabled = true
            self.minorIDTF.borderStyle = UITextBorderStyle.RoundedRect
            
            self.uuidEXLBL.hidden = false
            self.majorIDEXLBL.hidden = false
            self.minorIDEXLBL.hidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255, green: 82/255, blue: 139/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.noteLBL.hidden = true
        self.beaconManager.delegate = self
        self.beaconManager.requestAlwaysAuthorization()
        
        self.uuid1TF.keyboardType = UIKeyboardType.ASCIICapable
        self.uuid2TF.keyboardType = UIKeyboardType.ASCIICapable
        self.uuid3TF.keyboardType = UIKeyboardType.ASCIICapable
        self.uuid4TF.keyboardType = UIKeyboardType.ASCIICapable
        self.uuid5TF.keyboardType = UIKeyboardType.ASCIICapable
        self.majorIDTF.keyboardType = UIKeyboardType.NumberPad
        self.minorIDTF.keyboardType = UIKeyboardType.NumberPad
        
        self.uuid2TF.enabled = false
        self.uuid3TF.enabled = false
        self.uuid4TF.enabled = false
        self.uuid5TF.enabled = false
        
        self.equipmentNameLBL.text = appy.equipmentName
        
        // Fetching beacon information from parse for already assigned beacons
        let query = PFQuery(className: "BeaconDetails")
        query.findObjectsInBackgroundWithBlock { (beacons:[AnyObject]?, error: NSError?) -> Void in
            if error == nil{
                if let objects = beacons as? [PFObject] {
                    for object in objects {
                        if(self.appy.username == object["username"] as! String && self.appy.equipmentName == object["equipmentName"] as! String) {
                            
                            let uuidParts:[String] = (object["uuid"] as! NSString).componentsSeparatedByString("-");
                            
                            self.uuid1TF.enabled = false
                            self.uuid1TF.text = uuidParts[0]
                            self.uuid2TF.enabled = false
                            self.uuid2TF.text = uuidParts[1]
                            self.uuid3TF.enabled = false
                            self.uuid3TF.text = uuidParts[2]
                            self.uuid4TF.enabled = false
                            self.uuid4TF.text = uuidParts[3]
                            self.uuid5TF.enabled = false
                            self.uuid5TF.text = uuidParts[4]
                            self.majorIDTF.enabled = false
                            self.majorIDTF.borderStyle = UITextBorderStyle.None
                            self.majorIDTF.text = object["majorID"] as? String
                            self.minorIDTF.enabled = false
                            self.minorIDTF.borderStyle = UITextBorderStyle.None
                            self.minorIDTF.text = object["minorID"] as? String
                            
                            self.uuidEXLBL.hidden = true
                            self.majorIDEXLBL.hidden = true
                            self.minorIDEXLBL.hidden = true
                            
                            self.saveBTN.setTitle("Edit", forState: UIControlState.Normal)
                            
                            self.scanningSwitch.hidden = true
                            self.scanningLBL.hidden = true
                        }
                    }
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    // Method to identify the closest beacon
    func beaconManager(manager: AnyObject, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        
        if beacons.count > 0 {
            self.beacons = beacons
        
        if self.closestBeacon == nil && self.beacons.count >= 1 {
            self.closestBeacon = self.beacons[0]
        }
        
        if self.closestBeacon.major != self.beacons[0].major ||  self.closestBeacon.minor != self.beacons[0].minor{
            closestBeacon = self.beacons[0]
        }
        
        // Getting the closest beacon details
        if self.closestBeacon != nil {
            let uuidComponents:[String] = self.defaultUUID.componentsSeparatedByString("-")
            self.uuid1TF.text = uuidComponents[0]
            self.uuid2TF.enabled = true
            self.uuid2TF.text = uuidComponents[1]
            self.uuid3TF.enabled = true
            self.uuid3TF.text = uuidComponents[2]
            self.uuid4TF.enabled = true
            self.uuid4TF.text = uuidComponents[3]
            self.uuid5TF.enabled = true
            self.uuid5TF.text = uuidComponents[4]
            self.majorIDTF.text = self.closestBeacon.major.description
            self.minorIDTF.text = self.closestBeacon.minor.description
        }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string.characters.count == 0 {
            return true
        }
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        
        // Validating the textfields for beacon ID's
        switch textField {
            
        case uuid1TF:
            return prospectiveText.containsOnlyCharactersIn("abcdefABCDEF0123456789") &&
                prospectiveText.characters.count <= 8
            
        case uuid2TF:
            return prospectiveText.containsOnlyCharactersIn("abcdefABCDEF0123456789") &&
                prospectiveText.characters.count <= 4
            
        case uuid3TF:
            return prospectiveText.containsOnlyCharactersIn("abcdefABCDEF0123456789") &&
                prospectiveText.characters.count <= 4
            
        case uuid4TF:
            return prospectiveText.containsOnlyCharactersIn("abcdefABCDEF0123456789") &&
                prospectiveText.characters.count <= 4
            
        case uuid5TF:
            return prospectiveText.containsOnlyCharactersIn("abcdefABCDEF0123456789") &&
                prospectiveText.characters.count <= 12
            
        case majorIDTF:
            return prospectiveText.containsOnlyCharactersIn("0123456789") &&
                prospectiveText.characters.count <= 5
            
        case minorIDTF:
            return prospectiveText.containsOnlyCharactersIn("0123456789") &&
                prospectiveText.characters.count <= 5
            
        default: return true
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
