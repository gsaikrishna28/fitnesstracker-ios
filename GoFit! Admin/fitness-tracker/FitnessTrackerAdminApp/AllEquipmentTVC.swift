//
//  AllEquipmentTVC.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 7/27/15.
//  Copyright (c) 2015 Techbees. All rights reserved.
//

import UIKit
import Parse
import ParseUI

// This class represents all the machines available to a gym owner
// The gym owner can then select specific machines for his/her gym in YourEquipmentTVC view controller
class AllEquipmentTVC: UITableViewController, UISearchResultsUpdating {
    
    var appy:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var equipments = [String]()
    var selectedEquipments = [String]()
    var deselectedEquipments = [String]()
    var searchFilteredEquipments = [String]()
    var searchController = UISearchController()
    var flag:Bool = false
    
    @IBAction func cancelBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    // Method invoked when the user presses done button after selecting/deleting required machines for his/her gym
    @IBAction func DoneBTN(sender: AnyObject) {
        self.changeEquipments()
        self.addSelectedEquipments()
        if self.flag == true {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // The selected machines are added into parse table
    func addSelectedEquipments() {
        for each in self.selectedEquipments {
            let gymEquipment = PFObject(className: "GymEquipments")
            gymEquipment["username"] = self.appy.username
            gymEquipment["equipmentName"] = each
            gymEquipment.save()
        }
    }
    
    // The deselected machines are deleted form the parse table
    func deleteDeselectedEquipments() {
        let gymEquipmentQuery = PFQuery(className: "GymEquipments")
        gymEquipmentQuery.findObjectsInBackgroundWithBlock { (gymEquipments:[AnyObject]?, error: NSError?) -> Void in
            if error == nil{
                if let equipments = gymEquipments as? [PFObject]{
                    for equipment in equipments {
                        for each in self.deselectedEquipments {
                            if (equipment["username"] as? String == self.appy.username && equipment["equipmentName"] as? String == each) {
                                equipment.delete()
                            }
                        }
                    }
                }
            }
        }
        
        // The beacon associated with the machine is also deleted, when the machine is deleted
        let beaconQuery = PFQuery(className: "BeaconDetails")
        beaconQuery.findObjectsInBackgroundWithBlock { (beacons:[AnyObject]?, error: NSError?) -> Void in
            if error == nil{
                if let beacons = beacons as? [PFObject]{
                    for beacon in beacons {
                        for each in self.deselectedEquipments {
                            if (beacon["username"] as? String == self.appy.username && beacon["equipmentName"] as? String == each) {
                                beacon.delete()
                            }
                        }
                    }
                }
            }
        }
        self.flag = true
    }

    
    // Method that calculates what machines have been selected/deselected by the admin
    func changeEquipments() {
        var array1 = self.selectedEquipments
        var array2 = self.deselectedEquipments
        var array3 = self.selectedEquipments
        var array4 = self.deselectedEquipments
        
        for var i=0;i<array2.count;i++ {
            for var j=0;j<array1.count;j++ {
                if array2[i] == array1[j] {
                    array1.removeAtIndex(j)
                    j = array1.count + 1
                }
            }
        }
        
        for var i=0;i<array3.count;i++ {
            for var j=0;j<array4.count;j++ {
                if array3[i] == array4[j] {
                    array4.removeAtIndex(j)
                    j = array1.count + 1
                }
            }
        }
        
        self.selectedEquipments = array1
        self.deselectedEquipments = array4
        
        self.deleteDeselectedEquipments()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255, green: 82/255, blue: 139/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.searchController = ({
            
            if let superView = searchController.view.superview
            {
                superView.removeFromSuperview()
            }
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.placeholder = "Search Equipment"
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        self.tableView.reloadData()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
        
        // Fetching all the machines information from parse
        let query = PFQuery(className: "AllEquipment")
        query.findObjectsInBackgroundWithBlock { (equipments:[AnyObject]?, error: NSError?) -> Void in
            if error == nil{
                self.equipments = [String]()
                self.tableView.reloadData()
                if let equipments = equipments as? [PFObject]{
                    for equipment in equipments {
                        self.equipments.append(equipment["equipmentName"] as! String)
                    }
                    self.equipments.sortInPlace{$0<$1}
                }
                self.tableView.reloadData()
            }
        }
    }
    
    deinit{
        if let superView = searchController.view.superview
        {
            superView.removeFromSuperview()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if self.searchFilteredEquipments.count>0 {
            return self.searchFilteredEquipments.count
        }
            
        else if self.searchController.searchBar.text?.characters.count > 0 && self.searchFilteredEquipments.count == 0 {
            return 0
        }
        else {
            return self.equipments.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Configure the cell...
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        cell!.accessoryType = UITableViewCellAccessoryType.None
        
        if self.searchFilteredEquipments.count>0 {
            cell!.textLabel?.text = self.searchFilteredEquipments[indexPath.row]
        }
        else {
            cell!.textLabel?.text = self.equipments[indexPath.row]
        }
        
        // Fetching all machines information form parse
        let query = PFQuery(className: "GymEquipments")
        query.findObjectsInBackgroundWithBlock { (equipments:[AnyObject]?, error: NSError?) -> Void in
            if error == nil{
                if let equipments = equipments as? [PFObject]{
                    for equipment in equipments {
                        if (cell!.textLabel?.text == equipment["equipmentName"] as? String && self.appy.username == equipment["username"] as? String) {
                            cell!.accessoryType = UITableViewCellAccessoryType.Checkmark
                        }
                    }
                }
            }
        }
        
        self.changeEquipments()
        // Placing a checkmark over the selected machine
        for equipment in self.selectedEquipments {
            if cell?.textLabel?.text == equipment {
                cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
        // Removing the checkmark over the deseleced machine
        for equipment in self.deselectedEquipments {
            if cell?.textLabel?.text == equipment {
                cell?.accessoryType = UITableViewCellAccessoryType.None
            }
        }
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        
        if cell.accessoryType == UITableViewCellAccessoryType.None {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            self.selectedEquipments.append((cell.textLabel?.text)!)
        }
            
        else {
            cell.accessoryType = UITableViewCellAccessoryType.None
            self.deselectedEquipments.append((cell.textLabel?.text)!)
        }
    }
    
    // Method that implements the search functionality
    // The admin can search for machines using the search bar on the top
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.searchFilteredEquipments.removeAll(keepCapacity: false)
        for equipment in self.equipments {
            if (equipment as NSString).containsString(searchController.searchBar.text!) {
                self.searchFilteredEquipments.append(equipment)
            }
        }
        self.tableView.reloadData()
    }
    
}
