//
//  RegEx.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 10/2/15.
//  Copyright © 2015 Techbees. All rights reserved.
//

import Foundation



// This class uses regular expressions for textfield validations
class Regex {
    let internalExpression: NSRegularExpression
    let pattern: String
    
    init(_ pattern: String) {
        self.pattern = pattern
        try! self.internalExpression = NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions.CaseInsensitive)
    }
    
    func test(input: String) -> Bool {
        let matches = self.internalExpression.matchesInString(input, options: NSMatchingOptions.ReportCompletion, range:NSMakeRange(0, input.characters.count))
        return matches.count > 0
    }
}