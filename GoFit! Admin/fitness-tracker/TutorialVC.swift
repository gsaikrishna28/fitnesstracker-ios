//
//  TutorialVC.swift
//  FitnessTrackerAdminApp
//
//  Created by Admin on 11/11/15.
//  Copyright © 2015 Techbees. All rights reserved.
//

import UIKit

class TutorialVC: UIViewController, UIPageViewControllerDataSource {
    
    // MARK: - Variables
    private var pageViewController: UIPageViewController?
    var pageController:UIPageViewController!
    // Initialize it right away here
    private let contentImages = ["1.png",
        "2.png",
        "3.png",
        "4.png",
        "5.png",
        "6.png"];
    
    var tutorialPageTitles = NSArray(objects: "Enter your gym info and tap on Save.",  "The Equipment Tab lists the machines you have selected. Tap on + button to add machines.", "Select required machines by tapping and then on Done. You can also search using the search bar.", "After adding machines, tap a machine to add a beacon to it.", "Enter details manually or use the toggle button and tap on Save.", "To logout, tap on the door button!")

    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
//        super.viewDidLoad()
//        createPageViewController()
//        setupPageControl()
//        pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        createPageViewController()
        setupPageControl()
        pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50)
    }
    
    private func createPageViewController() {
        
        pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.blackColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1.0)
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemVC
        
        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemVC
        
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> PageItemVC? {
        
        if itemIndex < contentImages.count {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ItemController") as! PageItemVC
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.tutorialScreenTitle = (self.tutorialPageTitles[itemIndex] as? String)!
            return pageItemController
        }
        
        return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }

}
